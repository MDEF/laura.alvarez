THE JOURNEY

**00 Zero project**

Well, at the beginning of the Master’s programme with the development of the reflection for *Mdef Bootcamp* the connections that I tried to create in my brain were those ones that could respond to this set of questions:

· How could we sustain all the design industry without wondering and caring about the future?

· How can we create all of this for the present without having our eyes on the planet?


**01 One project**

*Engaging Narratives* was an excellent excuse to articulate our thoughts by creating a narrative. I realized that my area of interest was undefined, halfway between the future of fashion and the future of education. What I articulated in that moment was:

· Future of fashion? Perhaps its future has to face the challenges by creating more accessible, efficient and sustainable wearables, what means that needs innovative solutions that responses to the dialogue that the fashion industry needs to start surrounding its impact to the environment but also to humans.

· Future of education? Many people have been working in this field and developing amazing scenarios, which lead me to think that the real need here is to join the hands, to collaborate in creating a sustainable educational kit for the world’s environment. Perhaps the solution to this problem is the creation of a community where designers, activists, politicians, engineers, researchers, artists and scientists could join together and create this inclusive and innovative future for communities with severe lack of resources.


**02 First term final project**

For this final presentation I decided to explain how had been the journey of getting there and what I had experimented and discovered. Furthermore, I split the presentation into two parts. One of them was a video co-created together with Adriana Tamargo, student from the Master’s course, and the other part was mainly focused on the explanation of my journey, a set of collection of the gaps that I could identify in both fields and possible future and scenarios for both.

What concerns to the video it was a speculative future where waste was converted into new useful materials and then, reintroduced to the loop.

What I would emphasize most about my articulation of thoughts within the future of fashion is that to change the reality in the fashion industry it will be needed to open up the opportunity to unembedded fashion and style from our lives and become more conscious about what we wear, how we wear and the most important one, why we wear.  

“Do you wear what you think or do you think what you wear?”

The main topics then will be:
· Empower the audience
· Achieve a longer-term change of behaviour by focusing in slow fashion which leads to feel this new era by recuperating from the fast fashion
· Redesign habits

Speculative Interventions Proposed:
· Focused in one of the most complex human problems that may exist, the access to water and its waste. Cleaning clothing is such as a fact that creates a huge amount of waste, the energy spent by washing machines and also all those communities in which they don’t have water accessibility. In order to face this, I can imagine creating clothing that cleans itself.
*How can I learn lessons from nature? How can I be inspired by nature?*

· In order to generate inclusive moves, I asked myself:
 *What if I can create a wearable that  indicates the emotional state of any child with autism or any disability that doesn’t allow the person to communicate with normal language?*
It will be a wearable capable to transform its matter and also its color depending on how the wearer wants to communicate. No words needed for communicating anymore. Communication by intangible connections.


On the other hand, regarding the future of education, I articulated some of the actions/interventions that could be made such as:
· scrapyards to recover: network of professionals that work together to develop a system capable of reuse, recycle, make, remake the scrap.

· materials research: research materials that have great properties with a zero waste approach to face huge challenges for the society and material literacy to create an architecture of its transmission and give them solutions to grow by themselves.

· accessibility of technology: with the purpose of generate economy from the inside, locally manufactured.


**REFLECTIONS ABOUT FINAL PRESENTATION**

The feelings I got when presenting, during and after was the same. I had pretty clear in which areas of interest I wanted to articulate my narrative, which was the slowness of my project development as I was focusing in two of them at the same time.

Although I couldn’t articulate an specific area of interest and develop it further, I felt such as if it helped me a lot to get to know my thoughts while writing and presenting.

Then, it was quite helpful and significant for the next steps that I have done in my research.


**FEEDBACK**

In general, the feedback about my presentation was about thinking twice in terms of interventions and be more specific about what I really want to solve. I researched a lot and identified many different gaps, which I need to concentrate just in one challenge as through the Master’s program I won’t be able to develop more than one. So then, be more specific, precise and tangible and contextualize it better.

· Go specific
· Go local

· Get started
· Interviene the problem
· Generate conversations




**03 Mid term reviews + Mariana review**

I will split this mid term review in some questions that have been articulating my narrative during the course:

1) How can I help the fashion industry to evolve?
-- Fashion has stagnated. It is based in the ego of the consumer, an unsustainable pattern of consumption, which will have an economic impact in the longer term.
-- Perhaps future of fashion will not work within a cyclical trend, instead, it will relay in the identity from the person. It will be personalized.

2) How can I create an environment for the human being in order to become the active player in fashion? (Agencies)
-- Active player: adapting fashion to the human being.

3) How can I think about the evolution of the fashion industry by understanding it as the convergence between the smart, the technological and the sustainable fashion?

4) Is the relation between fashion and technology sustainable?
	-- Wearables such as intelligent exterior garments which can be applied.
          -- It should perhaps have more sense to apply technology in the process of production themselves, and not such as an addition.
5) What does it mean “sensorial intelligence”?

6) How can I learn through my senses more than from concepts?
How can I use the sensorial intelligence such as a way of awakening minds and getting to conclusions?
	-- Interviene myself.
	-- Interviene others as a learning experience.

7) How can I approach education in a sensorial based learning?

8) How do we, I, the human being comprehend beauty and esthetic?
             -- Normativity, rules, values, categories… What represents what?
	-- Which is the relation of this with ethics, morality and conventions of the system?

9) How can I integrate the data that the environment provides to us (stimulus, connections and senses) in the construction of my identity through the fashion industry?

10) How can I tie up the material world, the history and story, the traceability from the garment, in order to make aware the wearers about the reality?
	-- Perhaps sensorial education can help to teach the story, to create a different universe, to generate a new senses capable of connecting with the garment. Broke the gap between fashion and reality. Dissociation never was sustainable.

11) How can I connect identity with the material world in which these elements take place?

12) How these elements shape this identity in order to generate a relation of senses appreciation and sustainability?
