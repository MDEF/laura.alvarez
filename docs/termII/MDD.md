"Designers should be concious on the fact that, each times they engage themselves in a design process, they somehow recreate the world"

· Faculty: [**Mette Bak-Andersen**] [**Thomas Duggan**]


A) Theory
#1. Introduction to MDD.#

**Sustainability:**
The in-depth knowledge of materials and how work with them, have been pushed out. This knowledge gap represents a major issue when designing sustainable physical objects (and in many other aspects):

- Up to 80%of sustainability impacts are decided at the product design stage.

- This effectively means that the designer is the creator of a recipe and will unavoidably make decisions that follow the product through its lifecycle.

Therefore, the aim of this course is to bring the material dialogue which distinguished traditional craftsmanship back in to the contemporary design process.

Material driven design:
         *This brings us in to a field defined by art, natural science and technology where designers manipulate, grow or develop the material for a product in the same process as designing form and function*


The aim of this course is to produce a 100% biodegradable by searching for a waste material.
- A physical prototype of a product, made in the ‘real’ material as well as key material samples from the process.
- A lab journal, documenting experiments in text and pictures and the final material recipe.

. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

B) Practice
#2. Carving fresh wood.#

Good morning Valldaura! Nice to see u :)
![](images/MDD_GM.JPG)



**Location: Valldaura Self-sufficient Labs**

 About: *The Valldaura Campus is a large park and testing ground for innovation that features the latest technologies in the fields of energy, information and fabrication.* (More info available here: http://valldaura.net/)

 ![](images/MDD_1.png)


**Going for a walk around Valldaura surroundings**
 Learning: about trees, forest science & natural raw materials

 ![](images/MDD_T.png)

 ![](images/MDD_W.JPG)


**Working with the material dialogue in Practice using wood**
 Learning: about tools, how to use them & types of wood available

 ![](images/MDD_E.png)
 *explanations and work place*


 ![](images/MDD_K.png)
 *Tools available*


 ![](images/MDD_WA.png)
*wood availabe*


![](images/MDD_PW.png)
*hard workers!*


 ![](images/MDD_S.png)
 *Wood selected*




**The sppon** that came out from that beautiful piece of wood...

![](images/MDD_WF.png)

But... ¿how did it happened?

  After selecting the piece of wood I was feeling comfortable with, I went into carving with a small knife. After all my intentions of working with the small one, the one that really worked with me and my piece of wood was the best one:


![](images/MDD_H.JPG)

Basic steps I followed:

1) Selection of the wood
2) Going hard with the good carving knife
3) Change the tool: take the axe
4) When getting some kind of form, start modelling. Still with the axe.
5) Detailing with an knife and at some point when the wood asked for it, using a chisel.
6) Finishing with the gouge to make the whole clean.
7) Burning the piece of wood to create the final detail of the spoon.
8) Be happy after 6h of full pleasure, delight and joy although some difficulties appeared. haha.



**X) R e f l e c t i o n s about the process**

· The knowledge about the material is such as crucial in order to experiment with it.

· It is all about how I was feeling with the tools and with the material. Despite of being taught about the different tools, how to use them and the types of wood, I decided to go for a robust piece as I felt I had to work with it. Its beauty somehow captured my attention. In terms of selecting a material I learnt that for me it is better to be connected with what I am selecting in order to be motivated while experimenting it.

· You don't know how it is going to end up when working through the material.

· The material drives you, your mind and your body. Whenever I tried to drive the material I found many barriers in order to achieve the result I had in my mind. When I was flowing with it, learning how it wanted to be crafterd, the result was much better and it taught me in a way that you can learn about it.

· Depending on the tool, the result will be different. Also, it were some tools that didn't work with my material thickness.

· The material guides you. It is quite nice to appreciate its way and benefit from its way of behaving.

· Getting to that stage of the brein, that conexion between the body and the brain, when you are unconcious. The material & your presence. Nice one.

· Now I REALLY APPRECIATE THE WORK FROM the traditional craftmanship within activities such as woodwork or any of them and all my love to all kind of craftmanship specialists. You are such as beautiful people making beautiful things.


I can truly say that this emotional connections with the materials are my thing, I am quite senstivie and sensorial so it was the perfect time, I enjoyed it so much. Thanks to Mette, Thomas, Jonathan & to the Valldaura atmosphere.

SUCH A GREAT EXPERIENCE. Everytime we learn how to research, the knowing of everything... but, What about the knowing how? There is no activity such as finding out how the materials behave... It can be described such as peaceful experience and time. Where your mind and body start to work together, one with the other without really realize.
Offline cognitiion: when the material makes you start looking at other things... This is awesome. I could stay in that state forever.


Good night Valldaura!
![](images/MDD_GN.JPG)






#3. Raw material proposal.#

**Arundo donax**, giant cane, is a tall perennial cane. It is one of several so-called reed species. It has several names including carrizo, arundo, Spanish cane, Colorado river reed, wild cane, and giant reed.

 - Usually it grows to 6 metres (20 ft) in height, or in ideal conditions can exceed 10 metres (33 ft).
 - The hollow stems are 2 to 3 centimetres (0.79 to 1.18 in) in diameter.
 - The grey-green swordlike leaves are alternate, 30 to 60 centimetres (12 to 24 in) long and 2 to 6 centimetres (0.79 to 2.36 in) wide with a tapered tip, and have a hairy tuft at the base.
-  Stem and rhizome pieces less than 5 centimetres (2.0 in) long and containing a single node could sprout readily under a variety of conditions

Habitat: grows in huge colonys along very humid areas.

Scientific classification edit
Kingdom:	Plantae
Clade:	Angiosperms
Clade:	Monocots
Clade:	Commelinids
Order:	Poales
Family:	Poaceae
Genus:	Arundo
Species:	A. donax

**Grabbing some material... Why this giant cane?**

![](images/MDD_0.jpeg)

Its selection was due to its waste. The arundo donax is usually removed and taken away when it has been planted for one year. Here in Barcelona, the use of this plant is managed to last for some time and after that is being removed. Some people do take advantage of it for different uses but it doesn't have yet an industrial re-use as a waste.

Also due to its **environmental impact** because the reed (Arundo donax) is an invader plant able to grow and reproduce in a wide range of environmental conditions but mainly humid areas. The reed, an abundant plant in the Iriverbanks around the world, alters the ground arthropods communities and it reduces the body size of these invertebrates in the natural habitats it colonizes, according to a study published in the magazine Biological Invasions.


**Technical qualities**
![](images/MDD_CQ.JPG)


**Uses all around the world**

 - Energy crop
 - Cultivation
 - Biofuel
 - Chemicals
 - Construction
 - Musical instruments


**People who are working with this raw material in Barcelona...**

*Canya viva:*

integral management of this resource with the creation of a sustainable and unique competente construction model

This material has been proven to work and is it resistant in terms of its pre-treatment properties? to replace heavy industrial machinery and build with your own hands architectural structures, build structures

- houses built with natural materiales
Clean material, does not contaminate

- shapes that you get with the arcos that you do not get with other material

- changes the typical structures of straight and square houses




- - - - - - - - - - - - - - - - - - - - - -

 R e f e r e n c e s:

https://partesde.info/

https://www.youtube.com/watch?v=vChaft6mYok

https://www.youtube.com/watch?v=bxToVf4drb4

https://www.youtube.com/watch?v=FOsSSFeosNs

https://www.facebook.com/ArteyArquitecturaOrganica/videos/todas-las-fases-del-m%C3%A9todo-canya-viva/1529798950428167/

https://www.sciencedirect.com/science/article/pii/S0304389407007509

https://www.sciencedirect.com/science/article/pii/S0960852409002739

https://www.sciencedirect.com/science/article/abs/pii/S1161030104000528

https://link.springer.com/article/10.1007/BF02860024

http://canyaviva.blogspot.com

http://naturconsejos.blogspot.com/2014/10/cana-comun-arundo-donax.html

https://www.fitoterapia.net/vademecum/plantas/index.html?planta=429

https://www.miteco.gob.es/en/ceneam/grupos-de-trabajo-y-seminarios/red-parques-nacionales/Bases%20para%20el%20manejo%20y%20control%20de%20Arundo%20donax_tcm38-169319.pdf

https://www.youtube.com/watch?v=DZCVS9s0hXM

https://www.sciencedirect.com/science/article/pii/S1359835X15002067

http://dspace.umh.es/bitstream/11000/2708/1/TD%20Garc%C3%ADa%20Ortu%C3%B1o%2C%20Teresa.pdf

https://en.wikipedia.org/wiki/Arundo_donax

https://www.sciencedaily.com/releases/2016/05/160526115814.htm


. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .


C) Lab Journal


#4. Exploration: about playing.#

![](images/MDD_2.png)

*image taken from Instagram*

This material is full of potential as it has multiple different parts to be taken out. It has a simple beauty itself. Because of this, the first step was to look at it in its pure state.
How can it be desconstructed such in a way that the whole parts of it can be kept on its natural form?

1) Split up in its different parts
![](images/MDD_4.JPG)
![](images/MDD_5.JPG)


2) Sampling processing
![](images/MDD_3.JPG)


![](images/1.jpg)

![](images/2.jpg)

![](images/3.jpg)

![](images/4.jpg)


![](images/MDD_BP.jpg)

![](images/5.jpg)

![](images/6.jpg)


![](images/7.jpg)

![](images/8.jpg)

![](images/9.jpg)

![](images/10.jpg)

![](images/11.jpg)





5. Learning: how can I take this further?
Next steps.

The next steps (in which we are working right now) are those ones that require the use of bone glue, testing it in different concentrations in order to create OBS boards by forming while adding adhesives and then compressing layers of wood strands in a specific orientation.

The final goal is to create a curve layer in which people can sit down and enjoy their time while working, eating or whatever.

The thing that we have in mind is something cute as the one design bellow, but let's just play and see what happens.

![](images/MDD_e.jpg)


6. Final design.

Pre-treatment
**A spoon made out of the Skin and Stem**

![](images/FDC.JPG)

![](images/PFD.jpeg)

![](images/PC.jpg)


Post-treatment
**Bioplastic: Bowl**
FINAL MATERIAL RECIPE so far:
![](images/MDD_FR.JPG)

PHYSICAL Sample:
![](images/fpp.JPG)


**Biocomposit: Layer of paper**
FINAL MATERIAL RECIPE so far:
![](images/MDD_FR1.JPG)


PHYSICAL Sample:
![](images/papel.JPG)


Next steps: Something I would like to work in with all the processes mentioned above and when taking it further with the bone glue testing and application:

![](images/zapatilla.JPG)



X) R e f l e c t i o n s   about the process:

  During the process we found out some difficulties such as the way that the cane had to be splitted into parts. It was quite rough for us to get to the point of having the material prepared for working through it. Although our material dialogue was quite inspiring as we found out many different useful parts that had great potential in terms of use as they are pure, while others had to be treated. When working with te material in its pure form, we could realize about its experiential qualities such as its smell and touch. When dried in the oven and while boiled it smells much more than when you grab it from the countryside. In terms of technical qualities, it is quite much easier to work with it after boiling, in terms of flexibility, as it can be molded and placed easily.

  When going further into sampling, we realized that going through bioplastics could be a nice idea for us in terms of feeling the material in 3D dimension. We decided to go quickly with it as we had some knowledge about bioplastic preparation. All the samples worked out after 2 days while drying in the outside and oven (low temperature). What we learnt about it, is that the best sample selected (which is the one recommended at the end) is when you make the bioplastic out of the fibers from the skin. This small fibers and a low proportion of extras adding can give the perfect recipe for molding something small, as is the bioplastic that gives more strenght, flexibility and structure.

  After trying out the bioplastic section, we start working with an non-extra components addding approach, which bring to us some new difficulties. What we learnt is that we should have focused into some key part from the cane because of the lack of time. We tried out many different recipes with its different methods, but our issue was trying it with all the parts we had available. As we were working with all the parts, we decided to try out them with the same bioglue. What we faced is that this bioglue didn't work as expected and we didn't have time to try out a different one such as bone glue (we are right now working on this process and we will submit the progress while getting insights). Another thing we found out was that we used the water from boiling for blending and for taking the fibers out, what we should have avoided. This made us not getting to a direction.
  What it realy worked out was the process of combining the different parts from the material and joining it itself, although we didn't find any property at all in all the samples.

  Some other challenges we faced was trying out the process while thinking about an industrial process after all. It was quite hard to find a way to implement this on an industrial process, as we did mostly all the time with some part from our body.

In terms of diameter:
· Longer - stronger - more flexibility - worse to glue itself
· Smaller - weaker - less flexibility - better to glue itself

The compressing process:
· it join them together and brings out the chance to use as less glue as possible

Be aware of the weakness due to the air inside the molds:
· The more compress the fibers the more strenght

The more water you use before the worst:
· we need to try to go slow with the water for making our composits or whatever we make, go little by little adding water  
