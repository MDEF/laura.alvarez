
“Atlas of Weak Signals”: a seminar focused in the construction of an Atlas of Weak Signs for the design of Futures. This special documented dataset has been built through both the analysis of the main crises that determine our collective futures and the implementation of methodologies of gathering, identifying and understanding those weak signals. The result of this seminar could be appreciated as THE ATLAS at https://kumu.io/sraza/materials6#aowsscraped, which has been made by Saira Raza linking weak signals with keywords scrapped from selected urls.

. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .


**Definitions_ Faculty: Mariana Quintero | José Luis de Vicente**
More info at: https://mdef.gitlab.io/landing/term2/04-Atlas%20of%20Weak%20Signals%20-%20Definitions.html

**Development_ Faculty: Ramón Sangüesa | Lucas Lorenzo**
More info at: https://mdef.gitlab.io/landing/term2/05-AtlasofWeakSignals-Development.html


. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

#THE OUTCOME#

Methods and tools
Corpus of information
New explorative strategies
Greater agility to scenario building
Collaborative development experience

. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

#THE WORKFLOW#

![](images/AoWS_Workflow.png)


#SESSION BY SESSION#

![](images/AoWS_Resumen.png)


. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

#WEAK SIGNALS?#

**What is a weak signal?**
Expecting that it will create some kind of trend. How to identify the things that are going to be important for the future.
Making it a trend or an affect or may just point to the complexities to a very big system

*A weak signal has a potential for a enormous change*

- recognize some possible effect
- make the connection: connecting a network effect


CARACTERIZAR WEAK SIGNALS:
Find material that lies outside the basic trends: anomalies
See secondary topics that came up from that basic trends.
*How can these secondary topics can go to the main topic?* content inside of them that is in the borders: find for the funny edges, on the fringes

- A weak trend (what) a trend before a trend
- A weak place (where) global/local
- A weak conversation/controversy (how)
- An intermittent start (when)
- A soft causality between scales (Why) Connection
- A set of powerless initiators (Who) outsiders/vision

Something that is not in the normal site [] no way to put a correlation line that catches the data [] pay some attention [] look at there and imagine some kind of networks that you can create there you can find a projection to the future

. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

Key concepts:
1) Find
2) Organize
3) Go visual

. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

#METHODS, TECHNIQUES AND TOOLS USED#

**Definitions**

1. Framework Diagram (Major shifts to come, Possible system directions - New possibilities)
https://drive.google.com/drive/u/1/folders/1mgB2DqVH2Sz0stm5xvkbkP74RzHzbnkp

2. 23 questions for radical scenario building: Combine the directions that we have. Exercising how to build for SCI. System flows and it is difficult to get out of them. Proposals, see them and see if it starts the conversation that we have for today: How do people... ?
https://docs.google.com/presentation/d/1Ol07PL90AZRZrtx3sQEX2krg2pgf_qyZnoo74XtS81U/edit#slide=id.p

3. Exercise for narratives creation and development (who, what, where, when, why, how - Headline, tittle, subtitle, lead, body, non-essential info)
https://docs.google.com/presentation/d/1YYwN7gtg067s4a_eamf6Yd3RnC-ZaWvlCZJEMAajpBk/edit#slide=id.p

4. Causes and symptoms
https://docs.google.com/presentation/d/1hnQxZOSFy9jNzXAw3tXCk1cyh2aseGFjmvKgbC7uhOg/edit#slide=id.p

5. Mission Model
https://docs.google.com/presentation/d/1xf5lKmCvfLhvva740yWRPTbxRfoyFBkNllonmIO_hdk/edit#slide=id.g57c3e1765b_0_25



**Development**

![](images/AoWS_GIF.gif)


1. Identifying and characterizing Weak Signals - Data sources sheet filling (interesting findings): topics researched on the google sheet document

2. Data scraping: Every week we went through all the process of characterizing weak signals, find links between controversy and friction. Understanding the entities space. Organize the information - Get the information from the website - Cluster information from the website - find links (algorythmia?...)

3. Semiotics map

4. Ontology map building - Kumu.io
https://kumu.io/


Find general methodologies: Managing weak signals:
- Scan
- Isolate
- Evaluate
- Organize
- Communicate

Tools: Guidelines/Methodological Inspiration
- network analysis:
- getting data
- text mining
- trends analysis
- topic models
- anomaly detection
- organizing: Ontology mapping, Wiki Database and Knowledge Federation.

Then we have to do Collaborative Organization - make sure everything is gonna be collaborative content - no limits - the facts in the data.


. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

#ABOUT THE SEMINAR#

[·] What I take from this seminar as designer for emergent futures


As areas of research I do consider all of the vectors of exploration quite interesting for looking at and taking advantage of the methodology and tools provided in the seminar to develop them further. In fact, I guess this could be a great activity for the whole Master’s program, not only for a couple of weeks, but to do this weekly. In a way, it forces you to work with some kind of pressure in terms of the assignments, then you have to solve in a short period of time and address complex topics while simplifying them by choosing just one trend. Perspectives are used every week so then you can be exploring one vector and one weak signal from a different view that you did not consider the last time. This brings to a research, in my opinion, wealth of information, ideas and knowledge.

Which of the 25 identified weak signals are personally more interesting to me as areas of research?


. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

#ABOUT MY PROJECT#
[·] What I take from this seminar for my final project development

Definiteviley the most interesting weak signals to my project as area of research are the ones that correspond to **IDENTITY** which we will be addressing after the break.

My project is focused in the transition between unsustainable identities and sustainable identities within the fashion industry. In a way, we are holding unsustainable identities because we are getting dressed from a system that is completely unsustainable on itself: the clothing that we wear is based on a unsustainable system called the fashion industry.

The environmental impacts such as environmental damages or the disposable clothing, the inhumane working conditions where people have rights limited or non-existent ones and the toxic chlothing that is produced by the use of chemicals which are damaging our health are the vectors that point to the final of a industry era based which leads to a unsustainable future. What I can appreciate here, though, is that the most important fact in my opinion to tackle the industry is by thinking which is the space that clothing takes out from our identity. The factors within the societal impact should be addressed.  

![](images/AoWS_CWS.JPG)

Which of the 25 identified weak signals are more interesting to my project as areas of research?




This project's aim is
addressing those factors within the societal impact that hinder the achievement of both individual and social sustainable identities...


#Which do you feel that particularly have an impact on you and your future? Why?#

. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

#FINAL PRESENTATION#

This presentation was about constructing a narrative using the *references* and *history about the topic of interest*. The narrative had to communicate effectively why the signal identified was a “weak signal” on itself, how we were going to address it and what we were trying to make.

**Why the signal identified is a weak signal**
Ontology Map
Link to the ontology map
Scenario
Link to the scenario
**How we were going to address it**
Development Plan
Link to the plan
New Digital System
Link to the new system
**What are we trying to make**
Interface
Link to the interface
New Mall
Link to the new mail

We were splitted in 3 groups according to our areas of interests/intervention. My group was focused in the interaction with the others within in cities.

Participants: Adriana, Alex, Ryota, Mayte, Julia, Vicky & Gabriela
Area of interest: Future of Cities
Possible topics intersections: new jobs, climate-consciousness, human-machine creative collaboration, redesigning social media, technology for equality, climate migration, breaking the filter bubbles and carbon neutral lifestyles.

<<<<<<< HEAD
=======

>>>>>>> f863a93a1c113b0db20102d8a6735d0bf58aae7b
For this final presentation it was a great opportunity to apply all the methodologies and tools that we were introduced to during the seminar.

Outcome
+ How to create the communication about complex topics
+ How speech and topics go together and why
+ How to articulate the definition and how it is connected to the next definitions


Links to the presentation
https://drive.google.com/drive/u/1/folders/1n3flP8-wSlmQrLZXja6JRV17b-bVaUbt
