Abstract
> Speculate the future to calibrate the present; experimentation has been the goal of every session. We have been able to experiment the future to understand the present circumstances and also we have got to know how to act depending on the failures when proving our concepts. In other words, it has been a week in which we have been provided with the knowledge that we need in order to create actions that help us to get a deeper understanding of something.

·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.

Taking *actions* for getting a *deeper understanding* of our area of interest;

**Meaning**

M A T E R I A L   -    S P E C U L A T I O N: DESIGN RESEARCH "researching by speculating"

· Contradicting one world to open up another possible world
· New approach that expands on the traditional one
· Explore the unknown
· Manifestation of a counterfactual in a material artifact: a counterfactual artifact (embodied proposition that encountered generates possible worlds and accounts to explain its existence)
· Investigate critically our world through the design of material artifacts thar are specially crafted for the purporse of enquiry

![](images/Materialspeculation.JPG)

Counterfactual artifacts(present)                ME                 possible worlds(future)


**Tools&Techniques**

"knowledge to comfront yourself"

"Not because we are not in the future we can't try it and test it know"

The tools and techniques will allow us to test the real world with our speculative object.

- How can we transform an SO to provoque something and then analyze what happened with these affections?

- How can you apply the techniques that you learned in the workshop before?

- How is going to be useful for you?

- How can we learn more about something we are concerned about?




**Understanding the speculation process through addressing a concept while experimenting**

· Living with the thing: framing objects and undesrtanding how they might Work

![](images/Monday.JPG)

We were asked to bring some of our everyday objects in order to create an inventory and then speculate with them. The process was the following one: take an object, make the new object (ideas), transform it, live with it, learn something different that you didn't know before. All the process was EXPERIENCE the object.

When you do speculations physicalizing objects you can disrupt and give a hint. The artifact will give you insights once you have transformed it. So that is the reason why these artifacts can create interventions by its transformation. The question though is: What can we argue with this transformation when in the future be deployed? Which is the effect of this new object? What does space/atmosphere experience with the new object?

![](images/libros1.JPG)

Well, I decided to grab this book as it looked so natural for me. It was nice to not have seen the objects before because when you choose is without overthinking, just something that you feel in that particular moment is going to work for you, by intuition. Somehow I have a special feeling with second-hand books so I got this one.

What I asked myself, concerned about the future of the physical objects and interfaces, was what will happen if all the existing physical books disappear? What if we live in a world where nothing physical will exist? We are living an era in which every physical interface is being substituted by its pertinent digital one. Nowadays, most of the people don't know how it feels to read a book and cross the pages, how it feels to smell the book or having it placed at home. The experiences that we live with the physical interfaces are much more valuable and meaningful for me than the digital ones. We are losing emotions by substitution with digital objects. Hence, in order to create a wider scenario, I thought what will happen if this will be the last physical book on the whole planet. Somehow it will be one of the most precious objects in the whole world. Where will be the book placed in order to be kept alive? How it will be kept alive? How could we preserve this book?

The connections between H (humans) - Technolgoy (increasing) - Physical Objects (disappearing)

![](images/LibroS.JPG)

In order to investigate and experience what will happen, I decided to create this scenario in which when you try to reach the book it disappears. I was trying to bring to life a discomfort for the people that were trying to reach the book. Which discomfort will feel the society about losing the last book on the whole planet?
Two scenarios were created: one in which people tried to reach the book so it disappeared and the other one when people were just contemplating it and it was kept alive, not destroyed.

After all, I tried to connect this feeling to critically understand how we are not taking care of everything physical around us such as the environment we live in.

When using it, every time a person tried to reach it the string was tangled what it represents whenever you don't take care about something it will be damaged.

The result, people used it and after some test, it couldn't be placed again as the string was broken.

What I understood is that if people don't know that is the last physical book on the planet they don't care. That's why it needs to be attractive in order to be desirable. It also needs an environment to be kept alive (a technology). Also, it happened, when telling them the story about the scenario, that some of them didn't act as I expected. Some didn't grab the string in the way that I thought they will do. Some of them "preserved it". It made me re-think how may people act when they know that we are losing our physical everyday objects and environment.

. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

· Wearing the digital: Embodying yourself physically

What would you wear? Embodying yourself physically: can it help you with technological innovations?

Trying different options to find emerging technologies that don't exist yet. Experience with the technology that doesn't exist yet is such an exciting way to imagine what I would like to wear.

ChromaKey app

![](images/E3.JPG)

Although it is a long-time technique it gave me some feedback. It requires time to understand how to organize the scenario, the atmosphere in order to get better results (light, position, background, images, contrast...). It made me feel as if I could wear whatever I wanted, in the way that the physical world doesn't allow you to do it, it makes you extend the possibilities of wearing. Tangible and intangible. My focus was extensions and wearing the environment such as the sky. Most of the times I couldn't get the result I was looking for but it worked in a way that made me feel the concept of it.


Some other test:

1. Playing with the app we can create some mannequins with changeability so it can help you to try different combinations in order to get the result that you want. You can try then different patterns, palettes, forms, textures, images...

![](images/CK.JPG)

![](images/CK1.JPG)

![](images/C.JPG)


2. Abstract painting in clothing

Jean-Paul Riopelle *painting* 1956

![](images/JP.JPG)



. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .


**Work in the studio to build the idea for our final concept**

· Creating a magic machine

What I ask myself/ How can I speculate but also how can I create something valuable and also sustainable?

![](images/MS.JPG)

*A trash bin that transforms leftovers into new materials to build your own clothing*

![](images/LF2.JPG)


Make leftovers+materials meaningful. Reuse of leftovers that the industry and the humans are creating in their everyday routines. Reuse of second-hand materials.

Possible world of transforming trash into textile and customize your clothing
inverting the course of the resources

![](images/IR.JPG)

What if I can eat and then wear the leftovers that I produce?
What if I could reuse my textiles?
What if we establish a fashion dialogue with the trash around us?
What if we could create a fashion item anywhere at any time?
What if we could reuse the trash to create our identity and personality?
Will we give more value to the trash we throw away without thinking twice?

Insights/

treatment of trash changes; the idea about the concept of trash changes completely; sense of identity
;sense of independence; illusion of creating; interactive; the connexion between feelings and clothing;dynamic wearables depending on the season; exploring, discovering and playing; RECOGNIZE RELATIONSHIPS THAT WE ESTABLISH FROM OUR DEVICE


the experiment/

looking around the room; looking for trash as new materials; start giving a different meaning to them; clothing in the possible world: giving value to the process and creation, not to the perception




**Future work**

Augmented reality labels that announce the belongings of the material, the procedure of how is it has been made, the employers that took part on it, the design behind, the effect of the people, the impact on society.

Drawings in the skin-simulating clothing and that can be modified by a screen.

The interactive painting that connect people by touching and giving insights.

Architectural forms with recycling materials that have sensors.
