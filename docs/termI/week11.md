Abstract
> This week was about making the first venture into the FabAcademy environment with the aim to get used to the machiens, get some knowledge in coding and get to know the digital fabrication processes that we will be using during the course. For that, we were asked to develop a project which could be atractive and playful.

·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.

For this seminar we were asked to split ourselves in groups and develop the project whitin them. My team composed of Alex, Adriana and Vesa decided to make a playful cats playground.

**This is how we named it:** "Unconventional cat furniture and playground that evokes the playful instict of the feline. This concept was created to make meaningful outcomes from the movement and footprints of the cat with an artistic output."

**The final idea:** "control a laser to make the cat follow it while is walking on a clay surface (a cat sculpting).""

As part of the seminar we were asked to document daily our process. Then, everything was documented there day by day.
Check the site here: https://mdef.gitlab.io/laser-cat/

![](images/W11cd.jpeg)


*Final reflections about the seminar*
In a way it was a playful and creative way to get introduced to the fab academy environment. We had the opportunity to *feel the vibe* that we will be getting once we start the course.
In terms of our concept, what we learnt is that it is important to keep in mind who is going to be the audience from the "product". Our potential customer was not interested at all in the space that we created for him, although we were pretty conviced that he will love it. x) Learning from failure, it was a great chance to understand how the digital fabrication processes work, we learnt how to work with the laser cutter properly by ourselves and the tests that need to be made with it before going further, how to prepare the files in order to be 3D printed. When coding, we had the change to really create something that contains input and output in arduino and the needs that goes besides it, such as an external battery. Also, it was pretty interesting to learn one from the other in the group as some of us were more into coding, others into video and others into something else. We could learn one from another and get a taste of knowledge for FabAcademy assignments. Hence, we got in contact with soldering for the very first time and it was pretty interesting! :)

And the whole thing, all together, we got in touch with the pulse of FabAcademy!: **RAPID PROTOTYPING + DOCUMENTATING THE WORK SIMULTANEOSLY AND PROPERLY IN ORDER TO BE REPLICABLE BY OTHERS!**
