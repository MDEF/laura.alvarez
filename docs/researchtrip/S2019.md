Abstract
> Previous perspectives will always be previous thoughts. Through an intense week getting to know the whole atmosphere from Shenzhen we had the chance to drive in depth through its local ecosystems. What refers to the circular economy in advanced manufacturing developed in Shenzhen, it is the neuralgic center for manufacturing operations in China and holds one of the most complex supply chains we have ever seen.


·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.

![](images/SH.jpg)

#S H E N Z H E N#

**Why a research trip**

As students from the Master’s Degree in Design for Emergent Futures we were asked about the possibility to visit Shenzhen, known as one of the most innovative and dynamic innovation ecosystems in the world. In order to connect with this other reality in a different place in the world, we travelled all the way and stayed there for a week full of new stimulus.


**About Shenzhen**

Shenzhen is a major city in Guangdong Province, China, located in the southeastern. It forms part of the Pearl River Delta megalopolis, bordering Hong Kong to the south and Dongguan to the northwest.

Shenzhen is considered as part of the most rapid growth cities from the world.

Its evolution is impressive: from being fairly no more than a fishing village to become the huge metropoli owning the main production areas. It was considered as an special economic zone in which the cost of the m2 was low and also the labour was. All the production systems were moved from Hong Kong to Shenzhen and here started what they call: Shenzhen, the silicon valley of hardware.

Summing it up, it is considered as the place for digital innovation, smart cities, agile and open source manufacturing, fast growth and open-minded.

What made this possible? The rapid reaction force from the Chinese government who stimulated by a national Chinese policy an accelerated development in terms of growth of economic activity and population with the objective of making Shenzhen the prototype of modern China.

![](images/SH1.jpg)
*Image taken from http://en.people.cn/n3/2018/0629/c90000-9475974-14.html: Shenzhen in past 4 decades*

**Observations**

What I could observe about their atmosphere was their ecosystem. Their way of working and being oriented to the market in terms of rapid fabrication and prototyping. Its network is something to take a look at:

- Manufacturing Services
- Financing
- Software Development

The mindset of the professionals and the skilled workforce fully oriented to approach their effort to what the demand asks for. Ask the audience what do they want and then produce it. I surprised totally myself when I realized how collaborative was everything happening. Competition means going further in terms of product attributes and properties. It is not about being unique and getting the biggest market share, is about getting the more data from the potential and current customers in order to achieve a better solution for their requests.

 Not needing to be unique: this lead to the point of speeding up investment in what works in terms of demand:

- their motivations are focused on efficiency and fast development and production
- they are not worried about being unique or owners of a distinction
- they have been focused in standardization and development more than in differentiation

All of this mentioned above provide them have with the capability of being flexible, adaptable, in terms of production. The approach of not exclusive is what made them evolve so rapid.

The evolutionary approach greatly appreciated in their attitude of no struggling on decisions. These dynamics where a system is open to be improved and looking for better solutions, highlighting their speed of change. Competition is always over cooperation, which helps them in their journey of making evolution to happen. A clear example is how they grow an idea and own it just for three months where the others appear in the market. This is one of the key evolutionary approach due to its fast development.

Shenzhen’s ecosystem in the world is created such a system in which they make a combination of hardware, software and artificial intelligence. A hub full of factories, component suppliers, service providers and skilled workforce.

Its citizens and the daily professionals seem like they are keen to help others to create symbiosis. Many of small communities are born in this city thanks to this. Their attitude of thinking that there is enough space for everyone who wants to collaborate and develop something else improved. In their daily life their attitude to foreigners is quite an interesting fact. Even without being able to speak in english, they are always keen to find the way to communicate and help in whatever they can.
Shenzhen manufacturing culture where any kind of product is available for any country with any budget thanks to their rapid innovation and knowledge in advanced manufacturing.

The place for the makers. Quite impressive the local maker movement in Shenzhen and Dongguan. The culture of Shenzhen ecosystem is based in this maker movement. SZOIL teaches basic fabrication and prototyping. With his founder, David Li, and being located in the border with Hong Kong which makes possible the easy connections with design firms that are established around.

The future of Shenzhen could be based in the incorporation of more design and branding approaches from what I observed. Emerging companies in design and branding are becoming popular to bring the items directly from the factory floor to the design studio to explore the horizons and possible new approaches from the international market.

Hence, for future development of projects, Shenzhen’s connections with local partners is such as a big discovery. A huge network of manufacturers, prototypers and makers where it seems that there may also be a scope for combining the critical thinking with the rapid system that is happening in their ecosystem, while combining the horizontal approach from Western countries with the vertical one from Shenzhen.


**related to the topic of interest**

Area of interest: Circular Economy

Topic then so: Circular Economy in Advanced Manufacturing

Focus: The group observed the extent that Shenzhen exploits the opportunities for a more circular approach to manufacturing.
- e-waste

- repair culture

Our analysis of the local ecosystem started focusing in what we all observed during the few days that we had to get to know its ecosystem. What we first decided to put our hands-on was mapping the local ecosystem from Shenzhen focusing on Mobile phones and laptops:
- The loop of scrapyards, recovering and repairing
- The knowledge needed to perform those tasks
- The process behind


What do they do different from Barcelona? How does it the chain loop works?  What about the makers? its knowledge? the accessibility to education? Which % of scrap yards is used for new features? How is considered sustainability and Shenzhen (or China) all together?

**Observations of the TOPIC**
Although most of the production is exported, there are quite a few products that are repaired in the city which have involved a huge amount of knowledge, skills and equipment mostly based in Huaqiang Bei area.
Final products are made out of different parts that are purchased in the different markets. It was quite common to observe how the different parts were sold separately to create the final design.
Fabrication ecosystem:  Shenzhen is not only a relevant place to focus on from the rest of the world but also for the other cities of China. In the way that they are working in Shenzhen has the potential to be exported by the rest of China and allow them to integrate the repair culture that we got to know.
Recycling such as waste copper and plastic chips that were being collected for reuse in the fabric.  
The repair culture where we found a coordinated cottage industry of phone device and component repairs.


**Reflections**
I had the feeling that they can teach us such in a different way that we didn’t contemplate before. In terms of learning from their ecosystem, where the products generate conversations themselves when they come through demand. Here is one of the key concepts to me in terms of applying the learning to the development of my project. Hence, observing them while working and learning how to incorporate the speeding up philosophy in which all of them are immersed.
In a way, I was expecting to see much more in terms of innovation around the city, what I didn’t see. I was imagining a city completed turned into a technological hub where it was no place for “the traditional and the common”.
It was fascinating connect with new cultures around making and innovating. Getting to know the maker spaces, the manufacturers and the prototypes that can be our colleagues and partners in the future. I could see it happen in terms of introduce of the design into the manufacturing process; insert ideas that we can develop in the natural design thinking process in their way of behaving. Apparently it looks like it could be made by putting together the theoretical approach and the practical approach. At the end, all of us are looking at the same.
During my time there, I had the feeling that hanks to this trip we can create new collaborations. Establish potential collaborations because of the fact of getting to know many different professionals who could be involved in the interests that I am developing in my project. My future collaborations could be related to some of them  in terms of their educational programs and their methodology in the makers movement and also in their future projects and interests in the evolution of wearables and fashion electronics.
Because I am interested in learning about educational programs all around the globe and in researching about the future of education and how could it be focused in the increase of the student’s potential I was quite surprised about some of their connections with Africa. I would be very happy if China could export instead of the Shenzhen style production their educational methods for makers. Instead of focusing in economic benefits, if they could focus in educational programs to see them installed in Africa, which could really help them to develop their country as they do so.
Whenever I make a new trip, the most significant part for me is how your mindset changes during your time there. It allows me to get to know its citizens better, to immerse myself with their culture and grab the essence from every place I put my feets on. It is quite important for me to meet locals and learn from their culture, from their way of living, to take a taste of the community where everything mentioned above is happening. It is quite surprising for me when walking from the centre where all the electronics bubble was going on to the “little neighborhoods” where the life was so slow that it kept us calm during the dinner or beer time.







. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

#The agenda in pictures&notes...#

![](images/1RT.jpg)


1. SINO-Finnish Design Park

Brief introduction to the Design Park...
![](images/S1.jpg)
Some of the designs...
![](images/S2.jpg)
![](images/S4.jpg)
![](images/S5.jpg)
Evolution...
![](images/S6.jpg)
Collaborators...
![](images/S3.jpg)



· CBON DESIGN CONSULTANCY
![](images/2RT.jpg)

3 principles
1 design for pure beauty
2 for requirement
3 drives business industry creates a prosperous nation

with a 15% rate of AI products and ''no sustainability trend right now''
many productions when they work with public_some companies sell a lot as the toothbrush working with the public:


investment by Chinese president - selling a lot - 100million of sells


· INPARE
Focused on different kind of designs...
![](images/3RT.jpg)
![](images/4RT.jpg)
![](images/GRT.gif)



2. SZOIL - Shenzhen Open Innovation Lab + FABLAB Shenzhen

![](images/sz.jpg)


Management team: David Li, Vicky Xie, Shirley Feng

**David Li**
Co-founder of Hacked Matter, Maker Collider and Shenzhen Open Innovation Lab. Focus on understanding and promoting open innovation, the Shanzhai style.

Vicky Xie
Global Cooperation Director for Shenzhen Open Innovation Lab

Shirley Feng
co-founder of Shenzhen Open Innovation lab

Consultant team: Neil Gershenfeld, Lyn Jeffery and Silvia Lindtner
Neil Gershenzfeld
Founder of Fab lab
Director of MIT's Center for Bits and Atoms

Lyn Jeffery
Director of Institute for the Future (U.S.A.)

Silvia Lindtner
Assistant Professor at the University of Michigan in the School of Information

"We advocate the creation
 We believe in the Innovation
  We are happy to share
   We are who we areasWe are the makers

Return to the essence of innovation
The world is changed cause of you"

 They are a space and platform for worldwide makers to communicate and cooperate. SZOIL is also the first Fab Lab in Shenzhen authorized by MIT CBA as a research and development partner of Fab Lab 2.0. SZOIL embodies four functions including Fab Lab promotion and Fab Lab 2.0 research and development, innovation and entrepeneur education courses for makers, global maker service platform and industry chain collaboration service.

 The lab dedicates ix exploring the issues and developing solutions to connect the massive production ecosystem to small hardware startups so as to promote the international standing of Shenzhen in de development of digital intelligent hardware and manufacturing and build a future intelligent hardware Silicon Valley by combining new open source method and current manufacturing system in Shenzhen.

 Objective: guide international makers to innovate with Shenzhen together

 Output: to explore a replicable and practicable mode of innovation eco-construction and collaborative Development

 Vision: To create a digital, innovative and intelligent factory. Fab factory which can unleash infinite creativity


**David Li**
![](images/DL.JPG)

Co-founder of Hacked Matter, Maker Collider and Shenzhen Open Innovation Lab. Focus on understanding and promoting open innovation, the Shanzhai style.

Smart hardware from streets_
A brief introduction
He is the founder of the first maker space in Shenzhen in 2011. New revolution makers and new things were born. He has pretty clear that making a movement like this at that time was possible because his integration approach, the open source hardware is an integration thing.

Today's Shenzhen Landscape from his perspective

...Coming soon



. . . .

The sky + the sun appear in Shenzhen... Human costs were visible: the quality of the air was completely schocking. But finally, we could appreciate the clouds, the blue color and the sunlight for a while...

![](images/5RT.jpg)

. . . .


3. Seeed Studio
![](images/12RT.jpg)

Everything starts from manufacturing_

Makers are hired for the industry, so they become a purporse together.
The company has been revolutionary for 10 years.
How can we use IoT to improve the efficience? Smart Hearlthware

Their vision_

MANUFACTURING as the NEW industry

AI is composed of hardware: AI (knowledge) + IoT (Data) and also of no hardware.

3D printing experimenting with new materials, 3D printing is going to be the nex service.

China as the factories of factories. THE FACTORY = Dongguan. This is the main concept: their purpose is to see Dongguan become the factory of the factories.

Descentralized manufacturing: getting closer to the customers brings benefits for everyone.

Open loop manufacturing: equipment

First, the project: look at manufacturing. Second, adapt the manufacturing to your project/idea. Then, GO BACK and START AGAIN.

PRODUCTION ON DEMAND, change on the way of we design.

Dongguan adjusts itselfs after landing.

Electronics = Art = Artistic

Makers: prototype & Descentralized
Open environment created in 2011: makers space place

Performance artist about making: Makers
Culture: change: young people thinking in a different way.

MAKERS as PARTNERS: Collaborations efforts: more power: influece: better seen

BIG MAKERS + INNOVATORS = key
__________________________________

THE WHY: from IT to IoT: The Wide Gap
IoT: Gap should be fixed. How? Their SOLUTION: Seeed - bridging the gap with IoT Ecosystem Partners:  open - modular - structured (software and cloud)

__________________________________

Innovate with China_
Talking about maker movement is talking about innovation: look at specific things. China could be the possible supply to acomplish that innovation.

Scale from prototype to Mass Development. Manufacturing know-how.
__________________________________

From maker to industries: invite make proofs: CLEAR NEEDS... X.FACTORY

__________________________________

Social benefit application
__________________________________

IoT solution: VERSION: Revisionable


4. X. Factory

![](images/xfactory.jpg)

They have such as an interesting space for makers in which I could see myself working or developing something. It is always nice to see the place where others create things and their explanations about their work. They had a great attitude towards innovation!

![](images/XF1.jpg)
![](images/XF3.jpg)

Nice reuse of leftovers...

![](images/XF.jpg)


Their presentation about their work...
![](images/XF6.jpg)


5. Urban Village by Jason Hilgefort
Jason Hilgefort studied at the University of British Columbia and the University of Cincinnati. He has worked with Peter Calthorpe, Rahul Mehrotra, Maxwan, and ZUS. He lead Maxwan’s competition victories in Helsinki, Basel, Kiev, Brussels, Ostrava, Hannover, and Lithuania before winning Europan 11 in Vienna. He subsequently founded Land+Civilization Compositions, a Rotterdam|Hong Kong based studio.
 He was a subcurator in the Shenzhen|Hong Kong Urbanism|Architecture Biennale and co-director of its learning platform. Currently, he is the International Director of FUTURE+Aformal Academy. He is a writer and contributor to SITE Magazine.
![](images/JH1.jpg)
He gaves us a short introduction about the urbanism atmosphere and its evolution
![](images/JH2.jpg)
![](images/JH3.jpg)
![](images/JH4.jpg)
![](images/JH5.jpg)
![](images/JH6.jpg)

6. Huaqiangbei
Custom molding shops + software developers

The streets, the outside..

![](images/HQ13.jpg)
![](images/HQ14.jpg)
![](images/hq8.jpg)
![](images/hq9.jpg)

The inside... Imagine that you could get ANYTHING, even things you never imagined before... Incredibly amazing environment and place to visit... and contemplate. I wish I could be closer to this "shopping mall" during the development of my project in fab academy...

![](images/Mall.JPG)

The skilled workforce...

![](images/HQ2.jpg)
![](images/HQ7.jpg)

Items for selling...

![](images/HQ6.jpg)
![](images/HQ5.jpg)
![](images/HQ21.jpg)


7. Chaihuo X.factory (Dongguan)

Beautiful atmosphere for makers!

![](images/C0.jpg)
![](images/C1.jpg)
![](images/C2.jpg)
![](images/C3.jpg)
![](images/C4.jpg)

Some projects...

![](images/C5.jpg)
![](images/C6.jpg)
![](images/C7.jpg)
![](images/C8.jpg)
![](images/C9.jpg)
![](images/C10.jpg)

The atmosphere...

![](images/C11.jpg)
![](images/C12.jpg)
![](images/C13.jpg)
![](images/C14.jpg)

![](images/C15.jpg)

Love their style in here...!

![](images/C16.jpg)






See u soon,

![](images/FRT.jpg)

. . . . . . . . . . . . . . . . . . . . . * . * . * . . . . . . . . . . . . . . . . . . . . . . . . .

References
  [1] http://www.sfdpk.com/index.php?r=sfdpk
  [2] https://www.szoil.org/
  [3] https://medium.com/@taweili
  [4] http://landandcc.com/profile/
  [5] http://en.people.cn/n3/2018/0629/c90000-9475974-14.html
  [6] https://www.technologyreview.com/s/612571/inside-shenzhens-race-to-outdo-silicon-valley/
  [7] https://asiasociety.org/history-city-without-history
  [8] https://es.wikipedia.org/wiki/Provincia_de_Cant%C3%B3n
  [9] https://www.youtube.com/watch?v=FJSN5hFLLxM
  [10] https://www.youtube.com/watch?v=FJSN5hFLLxM
  [11] https://www.youtube.com/watch?v=25yUA1gxyCU

Video under construction...
