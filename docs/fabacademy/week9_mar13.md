#EMBEDED PROGRAMMING

Group assignment
- Compare the performance and development workflows for other architectures

Individual assignment
- Read a microcontroller data sheet.
- Program your board to do something, with as many different programming languages and programming environments as possible.

*Missing pictures or video of your board programmed*
*Code*
·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.
*Learning outcome*
Identify relevant information in a microcontroller data sheet.
Implement programming protocols.


I worked together with Adri.



**Documented what you learned from reading a microcontroller datasheet**

EMBEDED PROGRAMMING:
> OBJECTIVE: Building interfaces
> This week objective: understanding the workflow

What I have more clear about embeded electronics week is that through them we will be available to build interfaces with a long-perspective, we need to really understand its workflow in order to achieve that though. Understanding by making is a good thing, but when you are starting from zero there is a crucial need of having clear before going hands-on some on the key concepts that are involved in this broad field.

We will be working with a microcontroller. As this week is about learning how to programm our arduino boards, we have to identify relevant information in Attiny44 microcontroller data shet and implement the programming protocols.

![](images/W9_AM.JPG)

About the **ATtiny44:**

- Small: **14 pins**
- Cheap: **~0.5€**
- Same family as most Arduino chips, AVR manufactured by Microchip formerly Atmel.
- The Arduino UNO uses the bigger ATMega range, mostly the ATMEGA328P.


**Visual layout** to understand it better:

![](images/W9_VI.png)


**Pin Layout** in the data sheet:

![](images/W9_PL.png)


Next thing I had clear is the different types of **memories** that exists. Registers, SPRAM, DRAM, EEPROM, FLASH and fuse.
**Pheripherals** are the ports, the pins from the microcontroller. It is quite rough its diagrams when you look at them in the data sheet. It's quite difficult to really undestand what is happening, what it is its workflow. When you look at those pins the thing is quite as they are independently a machine each of those. To use one of this pins means to understand a whole universe. Write the programm for it is writting for an universe.

In terms of **word size**, what I understood is that a bigger word size is better.

In terms of **families**, we will be focusing on AVR due to its design, its workflow as it is the other way around: the processor on the compiler. Is also a tool change in software design and an inexpensive one.

Adafruit seems to be an interesting option in terms of **vendors.**

We will be focusing on the AVR processor ATtiny 44A.

Each part has REGISTORS, every single section has one of those.

Packages: TSSOP

**Overview of the data sheet**


**Develop the processor**

In-system programm: we need a ISP HEADER which can
  1) connect programm to it
  2) clips under the processor
  3) hold on pins

  Arduino: load the programm and don't need it anymore cause you have a communication interface, then we have different workflows:

  ISP HEADER ------------------> Arduino
  Arduino ---------------------> arduino

Programmers we will be using: Arduino

**Assembly language** is what the processor understands. Hex file is the screen of the codes. Assemblers is where we write the instructions. We will be using such as a low level language call "C": computer language

**Arduino**
The great thing about Arduino is how many things it integrates or generates. It is also a low price and accesibility one.
- IDE
- board
- HEADER
- libraries
- Boatleader

workflow: 1) sketch  2) programm: -board, -programmer bootloader, -ISP connection, -serial interface to load the programm




**Programmed your board**

Microcontroller Data Sheet
https://docs.google.com/document/d/1AjxysgPJQ1X0N3smNhSx6yKmGEc6trmumc8wqB0zpsU/edit#heading=h.hua1vv6yv0tu

tutorial
http://highlowtech.org/?p=1695

1) Installing ATtiny support in arduino
- Archive/Preferences
copy this into the "Additional Boards Manager URLs" field: https://raw.githubusercontent.com/damellis/attiny/ide-1.6.x-boards-manager/package_damellis_attiny_index.json
![](images/W9_I.JPG)

- Tools/Board/Boards Manager
![](images/W9_GT.JPG)

- In the Arduino Software:
Board: “ATtiny24/44/84”
Processor: “ATtiny44”
Clock: “External 20 MHz”
Programmer: “USBtinyISP”

FabISP comunicación base
FTDI comunicación normal
2) Connecting the ATtiny44 to the FABISP
Provide power to the ATtiny and connect it to your programmer

FTDI cable both to the board and the computer.
- Con el FTDI cable tenemos que conectar las dos placas, la ATtiny y la FABISP. (que sustituye al AVRISP)

3) Running the Burn bootloader

**Described the programming process/es you used**

4) Programming the ATtiny44

Next, we can use the ISP to upload a program to the ATtiny:

Open the Blink sketch from the examples menu.
Change the pin numbers from 13 to 0.
Select “ATtiny” from the Tools > Board menu and the particular ATtiny you’re using from the Tools > Processor menu. (In Arduino 1.0.x, these options are combined in just the Tools > Board menu.)
Select the appropriate item from the Tools > Programmer menu (e.g. “Arduino as ISP” if you’re using an Arduino board as the programmer, USBtinyISP for the USBtinyISP, FabISP, or TinyProgrammer, etc).
Upload the sketch.
You should see “Done uploading.” in the Arduino software and no error messages. If you then connect an LED between pin 0 and ground, you should see it blink on and off. Note that you may need to disconnect the LED before uploading a new program.



**Included your code**


Some useful links I found quite nice to have a look at:

[1] https://www.digikey.com/product-detail/en/ATTINY44A-SSU/ATTINY44A-SSU-ND

[2] recommended tutorial: http://www.ladyada.net/learn/avr/

[3] data sheet: http://academy.cba.mit.edu/classes/embedded_programming/doc8183.pdf

[4] tutorial for the C language that we will be using when programming: https://learncodethehardway.org/c/

[5] arduino langugage: http://playground.arduino.cc/Code/BitMath

[6] serial communication in arduino: https://www.arduino.cc/reference/en/language/functions/communication/serial/

[7] link about make your own FTDI: https://www.arduino.cc/reference/en/language/functions/communication/serial/

[8] how to use the arduino with AVR - ATtiny: https://github.com/damellis/attiny
