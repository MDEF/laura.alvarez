#3D SCANNING AND PRINTING


Assignment
- Test the design rules for your printer(s) (group project)
- Design and 3D print an object (small, few cm) that could not be made subtractively
- 3D scan an object (and optionally print it)

·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.

![](images/W6_Port.JPG)


**Additive Manufacturing Technologies**

Thanks to https://www.3dhubs.com/knowledge-base/additive-manufacturing-technologies-overview for the gorgeous site where they have described all the additive manufacturing processes and its atmosphere.

All 3D printing processes can be grouped under the 7 families (according to ISO standards). All of them have their main building material (plastic, metal, sand or composites). The major manufacturers of 3D printing and Additive Manufacturing systems for each process are also described here in order to understand the whole atmosphere.


Understanding the **additive manufacturing**...

![](images/W6_AM.png)

Extrusion (FDM) ZFused Deposition Modelling: Most widely used technology. Builds parts by extruding lines of solid thermoplastic material, which comes in filament form. Nozzles follows a predetermined path laying down melted material at precise locations.

Principles of Additive Manufacturing:
· Revolution vs. time
· Curvature
· Shell Thickness
· Infill percentage

Gravity LIMITATIONS - Overhangs and Supports (limitations of support)
Printing Supports - Optimization Algorithms (importance of orientation)
Material consideration -



**Basic concepts**

SIZE AND ORIENTATION
1. Make sure you scaled model fits inside the printer
2. Make sure your model “sits flat” on the c-plane - this is very important!!!
3. If possible, orient the model so the most supported side is facing down
4. Scale your model to print-size before exporting the .stl or obj (mm units)

GEOMETRY-BASICS
1. 3D printers can’t make zero-thickness planes-everything needs a thickness
2. “Open” shapes are ok, but they need wall thickness - RECOMMEND .5MM MINIMUM
3. Models should be “WATERTIGHT” - if it’s not watertight, it’s not a solid, and won’t
print well
4. Clean up coincident geometry - don’t have shapes inside of other shapes

GEOMETRY - OVERHANGS AND SLOPED
1. Most common printers build up (or down) layer-by-layer and cannot print in mid-air
2. Cantilevers will need support material or may be printed sideways or upside down
3. Models should be “WATERTIGHT” - if it’s not watertight, it’s not a solid, and won’t
print well
4. If there is a part of the model that “hangs down” it will need supports or will not print

MODELING SOFTWARE
1. Rhino is recommended, Sketchup can work but needs work and careful attention to
surfaces
2. Revit is very much not recommended
3. AutoCad 3d is also not recommended, but can work
4. Check your model with analyze programs like Meshmixer

MODEL CLEAN UP
1. Surfaces need to be oriented correctly - to the “outside” of the model
2. Be mindful of and remove from the printing model, geometry that intersects other
geometry
3. Consider removing very small geometry for small - scale buil
dings- door handles, mullions, etc.


**To know**
- CONSIDER TOLERANCES!

**Testing the 3D printers**
- Describe what you learned




**THE PROCESS of 3D Scanning**

Kinect: is a line of motion sensing input devices produced by Microsoft.
· For Windows: Download and install the Kinect for Windows SDK (version 1.8).


Object to scan: headphones
Software: skanect

![](images/W6_So.JPG)


Steps:
- before scanning, define the volume of the object to be scanned
- while scanning, rotate on the axis without moving the object
- 360 degrees angle, top and bottom
- press regenerate

Advice: Defining a smaller scanning area (bounding box) according to the scanned object improves the quality of the scan.

- export the .stl file, but since the version is free, the export has a very low resolution
- the mesh doesn't come out perfectly closed, you can always fix it with Rhino (command MeshRepair)
- fix it with Rhino and 3dprinted yourself

Problems:
- scanning too close can give you some troubles: in termns of geometry

Steps:
- After scanning: Reconstruct, process & share
- share: local (Save or export), web (sketchfab) or 3d print (Shapeways)


![](images/W6_So1.JPG)

![](images/W6_So2.JPG)

![](images/W6_So3.JPG)


_______________________________________
After scanning...


Visual editor:

![](images/W6_So3D.JPG)


Rhinoceros:

![](images/W6_SoR.JPG)


Sketchfab:

![](images/W6_SoS.JPG)
________________________________________


Second try using the same workflow but scanning the body

![](images/W6_Sb.JPG)

1. Prepare: body (bounding box not necessary as we will rotate and fix)
2. Record: limit 19" and delay 5" - move 360 degrees and stop
3. Reconstruct: as default
4. Process:
- Quick: run
- Watertight: run

![](images/W6_3DS.JPG)


**THE PROCESS of 3D Printing**

![](images/W6_P.JPG)

What we need to get at the end:
"YOUR MODEL MUST BE CLOSED, SOLID, “WATERTIGHT” AND HAVE NO “PARTS” UNDER .5MM/.03” THICK"

W O R K F L O W:  DESIGN(Rhino/MeshMixer) - PREPARE(Ultimater Cura) - PRINT(Octoprint)


Designing the model: I decided to use the 3d scanning I got from myself for 3d printing it. I tryed to use Rhino and the MeshRepair command to close it up and fix it, also I tried some other options to have the mesh ready for printing but I failed. I am not that expert in Rhino to expend several hours to understand and fix the mesh.

![](images/W6_MR0.JPG)

![](images/W6_MR.JPG)


Then, I got the advice of using Autodesk MeshMixer due to its simplicity, convenience and how quiclky it fix the mesh.

Key commands:
- Edit: plane cut
*Note that it allows you to cut the mesh in seconds for getting a flat surface*

![](images/W6_MC.JPG)


- Analysis: inspector
*Note that it gives you all the info you need to know in order to get a proper printing afterwards. For instance, if you need to add some support...*

![](images/W6_MI.JPG)


Exporting the file: saving it: Ultimaker Cura works with STL
Machine: RepRap (2)
Noozle: 0.6 mm nozzle
Material: Filaflex is a flexible material that comes in different colours. Requires the use of a 0.6 mm nozzle. Speed 10mm/s Temperature 235ºC Hodbed 40ºC

![](images/W6_r2.png)


· Prepares your model for 3D printing: Ultimater Cura
*slices your model ready for print. You can preview it, scale it and adjust settings as you’d like*

PREPARE:

1. Scaling

![](images/W6_S.png)


2. Machine + Material

![](images/W6_M.png)


3. Print Set up - Custom

![](images/W6_PS.png)


*Note that switching to the ‘layer wiev’ in Cura we can check the individual layer paths generated by the software with each setting variation.*



4. Set up the printing values

______________________________
Basic settings are the general settings that you usually want to change. These settings have the most impact on the result.

- Nozzle size: The size of the hole in your nozzle. The FabLab uses 0.4mm nozzle by default, and 0.6 for faster printing or filaflex.

- Layer height: The height of each layer. For print quality and printing time this is the most important setting. Usual settings are 0.2mm for a low quality print. 0.1mm for a medium quality print. 0.06mm for a high quality print.

- Shell thickness: The thickness of the side shells, it has to be multiple of nozzle size number.

- Bottom/top thickness: The bottom/top thickness is the outer shell thickness on the top and bottom. it has to be multiple of layer height number

- Fill density: Cura fills the internal parts of your model with a structure. This grid is made for strength and to support the top layers. The amount of infill you want is influenced by this setting.

- Print speed: Print speed sets the speed at which the print is put down. The default of 50mm per second.

- Print temperature: The print temperature is the temperature at which you print. if you want to print faster you might need to increase the temperature.210 for PLA and 240 for ABS.

- Support type: Supports are structures printed below the print to support parts that otherwise would be unprintable. There are 2 options here, support structures that need to touch the build platform, or support structures that can also touch the top of your model. You can modifie some aspects in(…)

- Platform adhesion type: The platform adhesion type is a setting to help the printed object stick on the printer bed. Large flat objects might get lose from the printer bed because of an effect called warping. There is the option to use a raft, which is a thick grid under the object which scars the bottom of your print. Or a brim, which are lines around the bottom of your object and because of the larger area the corners are kept down. Brim usually gives the best results here as it does not scare the object. But it requires more room on the printer bed.

- Filament diameter: The filament diameter is the diameter of your filament. Reprap uses 2.9mm.

- Filament flow: The filament flow is a correction factor to make extrusion higher or lower than usual. Some systems or materials require a correction next to the usual diameter setting. This flow adjustment can be used for this.
_______________________________


Basic values that need to be modified in this case:

- Quality: layer height (0,2 mm), initial layer height (0,3 mm), line width (0,6 mm)
*Note that the initial layer height has to be always a little bigger than the layer height*

- Shell: wall line, top l, bottom

- Infill: density (20), pattern (grid)
*Note that we have to press infill before walls*

- Material: printing temperature (260), build plate temperature (0), diameter (2.85)
*Note that we have to enable retration*

- Speed: initial layer speed (15)

- Cooling
*Note that we have to enable print cooling*

- Support: build plate adhesion type (skirt)


Here there are the screenshots with all the values:

![](images/W6_1.png)

![](images/W6_2.png)

![](images/W6_3.png)

![](images/W6_4.png)

![](images/W6_5.png)

![](images/W6_6.png)


Once we have saved the GCode file in Cura we can send it to the printer via Octoprint

· Web interface for 3D printer: Reprap octoprint .22

![](images/W6_O.png)

Upload the file to Octoprint and save it locally

Values that matter:
- Filament (tool 0): 3.59m
- Aprox. print time: 2.5h
- Hotend target: 260 degrees

Way of preparing the machine:
- Clean the leftovers from before
- Set up the filament in the extruder if it not already setted it up (cut it 45 deg to make it easy to go in the extruder hole)
- Extrude for a bit of material on a side of the plate to clean the nozzle
- Extre a bit of material to check the values and temperature
- Press “Auto home”. If the plate is not aligned disable the stepper and move it to home manually to adjust and level it
- Extrude a bit more of material before running the print job
- RUN THE JOB AND PATIENCE...

*How to save it in order to go back whenever is needed: r2_CONTORNO.flex-gcode (namemachine_NAMEFILE.typeofmaterial-gcode)*

![](images/W6_P3D.JPG)

The result:

![](images/W6_RF.jpg)

![](images/W6_RF2.jpg)



Some Useful links:

[1] https://wiki.fablabbcn.org/RepRapBCN

[2] https://www.3dhubs.com/knowledge-base/additive-manufacturing-technologies-overview

[3] http://archive.fabacademy.org/2017/fablabbcn/group/3dprint.html

[4] https://www.youtube.com/watch?v=c8qLHwwjPDE tutorial for 3d scanning
