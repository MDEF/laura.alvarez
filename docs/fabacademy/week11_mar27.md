#INPUT DEVICES

_SECOND HALF STARTS..._

The second half of the Fab Academy programme is designed to build on the previous weeks. You will be synthesising information and implementing skills that you were introduced to in the first half of the programme and encouraged to integrate these into your final project proposal.

Group assignment
- Measure the analog levels and digital signals in an input device

Individual assignment
- Measure something: add a sensor to a microcontroller board that you have designed and read it.

·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.

http://fab.cba.mit.edu/classes/863.10/people/matt.blackshaw/week8.html
Pasos:
PCB
- interior
- traces

Milling

PROGRAMMING

I worked together with Adri.

**Demostrate workflows used in circuit board design and fabrication**
**Described your design and fabrication process of your OWN DESIGNED BOARD**
**Explain the programming process you used and how the datasheet helped you**
**Cut/solder/programm one of the examples to understand how it works and modify one or make a completely new one**
**Described your problems and how you fixed**
**Include your design files and photos**
