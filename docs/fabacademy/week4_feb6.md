#COMPUTER CONTROLLED_CUTTING

Group assignment
- Characterize your lasercutter, making lasercutter test part(s), making test part(s) that vary cutting settings and dimensions.


Individual assignments
- Cut something on the vinylcutter
- Design, lasercut, and document a parametric press-fit construction kit, which can be assembled in multiple ways. Account for the lasercutter kerf.

*Laser explain how drew the files*

·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.


![](images/W5_P.JPG)


Group assignment

- Characterize your lasercutter, making lasercutter test part(s), making test part(s) that vary cutting settings and dimensions.


**Machines available:**

1. Speedy 100 by Trotec:
*30 watt compact CO2 laser-cutting and engraving machine*
*working area of 600 x 300 mm, and a maximum workpiece height of 170 mm*
*it works with the files from the Cloud, Rhinoceros 5 design software and Trotec’s dedicated JobControl laser software installed*
*it can not cut glass, engrave it yes though*

2. Speedy 400 by Trotec:
*120 watt high productivity CO2 laser-cutter*
*working area of 1000 x 610 mm, and a maximum height of workpiece of 305 mm*
*maximum processing speed was 3.55 m/sec with an acceleration of up to 5g*
*it works with the files from the Cloud, Rhinoceros 5 design software and Trotec’s dedicated JobControl laser software installed*
*it can not cut either glass or stone, engrave them yes though*

3. 2000 Series Laser by MultiCam
*still learning*




**Materials**

Leftovers: 3 mm MDF, 3 mm cardboard.


**Programm used**

Rhinoceros as it is the CAD software installed in the computers connected to the laser cutting machines at Fab Lab Barcelona. It aslo can be used with Grasshopper via Rhino when designing parametric projects.

Notes:
-Be aware of the units settings. It could happen that when exporting it, although in your computer you already seted up millimeters that it appears centimeters in the computer from the lab. That's the first thing to change before start working your file. Otherwise all the work will mesh up.


**Communication Machine - Rhino**

The design to be printed has to be in vector format such as .dfx or .ai

. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .


**The Laser kerf test** (Machine used: Speedy 100 by Trotec)

Find out the laser kerf. Kerf is defined as the width of material that is removed by a cutting process.

Determinants of laser kerf: material properties, thickness, focal length of the lens, the cutting power, the cutting speed and/or the cutting frequency

How kerf widths can vary on the same material sheet.
Determinants: straight or curve line, X or Y dimensions, manufacturing tolerance.

Measuring the laser kerf: Testing the laser kerf of a laser-cutter could be as simple as cutting a 100 x 100 mm square and measuring the resulting part later with a digital caliper. The resulting part could be a 99 x 99 mm. square, meaning that the laser kerf is 0.5 mm.


- the slower the cutting speed, the bigger the laser kerf

![](images/W4_KT.JPG)



**Cutting, marking, engraving test parts** (Machine used: Speedy 100 by Trotec)
Another information that I already had at that time, was that the different processes of a laser-cutting job (engraving vs cutting) should be separated by color in order to differentiate them.

Cutting: red
Marking: black (hatch command)
Engraving: blue

Testing file in Rhino:

![](images/W4_CME.JPG)



P R O C E D U R E

1. Open the file in Rhino, make sure about the units (millimeters) and type SelDup to check if there are some duplicated lines that should be deleted. It is quite recommended to type Make2D, create its own layer and prepare everything in this layer (in case of just cutting - red color). In case of engraving or marking, another layer should be created.

2. Once the file is ready, execute the print command by typing print or either Ctrl + P. The printing setting programm will be open authomatically.

3. Printing programm:
- select the machine to use
- output type: vector for vectors, raster for bitmaps. When a file contains raster and vector, choose vector.
- scale: keep it in 1:1. it should be by default, if not, change it. set the same units for the file and the scale (1.0).
- set button: it allows you to move the working area relative to the design in Rhino. The way of selecting your working area should be always this: when you press set, it opens Rhino and appears the word MOVE, press it and then follow the instructions. Be aware of dragging the window from left to right, otherwise it will scale the whole design and it will lose its units.

4. When the Trotec Job Control opens. Drag and drop the job from the right side panel or double click the file. In case of checking if that one is the job to be working with, click once and it will appear a little window so you can check. Once is dragged, click on the job and you will be able to see a black pannel.

5. Use the materials settings set up to define the material print properties. Before setting the properties of cutting, marking and engraving it should be checked that the material is the same that the one you are working with. In case of cardboard, go to paper and then double click in cardboard. Then, start changing the variables. Be aware of not going more than 1.0 for the Speed value for any of the colors! Be aware of turning on the fan!

- The variables we can play with are power, speed and frequency. We should test and reference the engraving speed. The rule of thumb is to start slow and go up from there. This step is really a trial and error process as there are not appropriate settings for each material, it depends on the finish and result you are looking for, although there are many standard settings forms that could be helpful to get started setting the values for each material.

- We place the material in the machine and manually adjust the height until the small plastic thingy falls down.

- Manually place the laser in the starting point and send job. (Right-click on design to do a Job Reset)




. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

Individual assignments

- Cut something on the **vinylcutter**

To understand how this machine works there are some concepts I got to know before cutting something on the vinylcutter.

The first step is that we can choose between two of the vinyl machines that we have available at Fab Lab Barcelona. We have the CAMM-1 GX-24 vinyl cutter which is authomatic and another manual one (the one I will be using during the project).

What matters when working with these two machines is its width, the thickness of the material and understanding that the limits of the thickness. It is a rapid cutting tool which is quite easy to use.

Some of the things that can be made out of this machine: stickers, flexible circuit boards, a textured surface/relief pattern, screenprint resists/stencils.


How it works

1. Create your files: it is important the way that the files are drawn.

Using Inkscape for vinyl-cutting: from jpg: Object to path - Shift + Ctrl + F: fill and stroke

![](images/W4_VC2.png)


2. Send the files to the Cloud in the correct format: DXF ARCHIVES


3. Prepare the vinylcutter and cut something

![](images/W4_VC1.png)

Material: a piece of black vinyl cutter that was at the lab

Place the black vinyl piece in the roller base and place it properly with the mini lever that is on the right side. Once the adhesive roll piece is correctly aligned, the next step is to place it with a 90-degree angle so then you don't lose your file when cutting. For this I used the two pinch rollers (right and left) on the edges of the material and the mini lever. It is important to doble check the program while fixing the black vinyl piece to make sure where is it going to be cutted.

When prepared, turn the machine ON and the next step is pressing load and press ''the beggining button'' that is showed in the screen of the printing program.


![](images/W4_VC3.JPG)


4. Working after Cutting

![](images/W4_VC4.png)

Next step: stamp a t-shirt with the design from above!




. . . . . . . . . . . . . . . .

- Design, lasercut, and document a **parametric press-fit construction kit**, which can be assembled in multiple ways. Account for the lasercutter kerf. (Machine used: Speedy 100 by Trotec)


**Press-fit join test**

test part to find out the right slot size to achieve this perfect friction joint with the different materials.

![](images/W4_PFT.JPG)

![](images/W4_PFT1.JPG)

![](images/W4_PFT2.JPG)

For the 3mm MDF, the best press-fit joint was achieved using the 2.95 mm slots.



**Living Hinges**

I am in love with this thin flexible hinges. It is a simple and low-cost way to create connections between two rigid parts.

![](images/W4_LH0.JPG)

![](images/W4_LH1.png)

![](images/W4_G1.gif)



**Construction Kit**

For the construction kit I decided to combine the living hinges with a simple parametric design. I had never used before a parametric program so it was completely new to me. I tried to manage in Grasshopper but I am still learning. I could manage to get an polygon and set the parameters that I got while making the press-fit test.


This is one of the pieces that I created:

![](images/W4_LH.JPG)

![](images/W4_G2.gif)


This is how it looks all together for now, as I would like to create something bigger:

![](images/W4_G3.gif)
