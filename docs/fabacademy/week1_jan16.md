#PRINCIPLES AND PRACTICES

Assignment

- Plan and sketch a potential final project.


·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.

Currently I am part of the Master's Programm in Design for Emergent Futures at IAAC (Institute of Advanced Architecture of Barcelona). During the first term of the programm I was able to articulate some possible areas of intervention concerning to my areas of interest: the future of wearables and the future of education.

Fab Academy is such as the proper way for me to start learning hands-on and also to develop one of the essentials components from the thesis of the Master: the INTERVENTION part. I have never tried or been in contact before with digital fabrication so it is going to be a huge challenge for me, I am more than ready though. Concerned about the fabrication (r)evolution where it allows me to do something, once is done learn about it and then be able to do it in my own way, I would like to develop this week assignments (miniprojects) whithin an atmosphere where I am able to make things that can make me experience the intervention of the Master, the kind of future that I am imaginationg by exploring possibilities.

"the way we interact with the world is lead to the fab academy movements..."

It would be nice to declare a very clear intervention but I am afraid that it will change for sure. At the moment, my ideas come up when I try to asnwer the question: **what needs to be put in the world?**

I do believe that what needs to be put in the world are connections between the sustainable fashion and the smart fashion world. That is why I would love to create something, perhaps in form of a product, that can make this connection possible. I would like to give it a functionality but also I will be happy if it can be expressed through art. I want to create something that can help to become more concious about **what we wear, how we wear and why we wear.** So the aim of my creation will try to represent the meaning of it through a motion product or an art installation.


. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .


...Connections w/ nature through wearables...

![](images/CN1.jpg)
![](images/CN2.jpg)


I want to build something that involves nature in itself. I am fascinated about the invasion of nature everywhere.

 ![](images/p10.jpg)
 ![](images/IN.jpg)

Nature, in its wisdom, despite the human being, sooner or later everything returns to its place of origin. As may be seen, in many places around the world this happened, when nature, after suffered several ups and owns, environmentally talking or caused by the human, nature has recaptured its place.

  ![](images/IN.jpg)


Tunnel of love (Ukraine): it hasn't been abandoned, it is still working but nature has created around a tunnel and everything coexists.

![](images/IN3.jpg)


Palacio de Angkor Wat (Camboya): the force of nature, here it has invaded this temple, in which the roots have incorporated themselves to the archaeological complex.

![](images/IN4.jpg)


Shopping Mall New World (Bangkok, Tailandia): when it closed it doors. A few years later, a fire destroyed part of the building and left it uncovered. And, a short time later, the monsoon rains flooded the lower floors. To combat the invasion of mosquitoes and other insects, the authorities decided to introduce fish, which have been reproduced over the years to create one of the largest urban ponds in the world.

![](images/IN5.jpg)


Ghost town de Kolmanskop (Namibia): The ancient city of Kolmanskop was built in the early twentieth century for diamond seekers who worked in the area. Soon it became a city in pure European style (with casino, school, hospital and large mansions), until the mineral began to be scarce and the city was abandoned in the mid-twentieth century. The desert has appropriated it, today, it is one of the favorite places in the world for seekers of abandoned sites.

![](images/IN6.jpg)


If nature invade wearables...

![](images/p9.jpg)


JUST GROW IT: IF NATURE MADE SHOES_ SHOES MADE COMPLETELY OUT OF NATURAL MATERIALS. HE CALLS THE SERIES "JUST GROW IT!"
Christophe Guinet was born in Paris. His latest project is a series of Nike shoes made completely out of natural materials. He calls the series “Just Grow It” similar to the Nike slogan “Just Do It”.
![](images/SNM.png)

yellow flowers... amazing
![](images/YELLOW FLORWERS.png)

![](images/p1.jpg)


I am also deep in love with transparency in everythimg, in a way that you can put together many different items which apparently doesn't fix together. I really love the way that coexist all together...

![](images/p4.jpg)
![](images/p5.jpg)


Some similar projects I get inspired of...


Naomi Campbell transparency shoes
![](images/p2.jpg)


![](images/p6.JPG)
![](images/p7.JPG)
![](images/p8.JPG)



https://vimeo.com/192134166
https://vimeo.com/223866550

Short documentary on artist Nicole Dextras and her project Weedrobes, where she makes clothing out of natural materials. Dextras is an environmental artist from Canada who works with ephemeral materials to accentuate the cycles of nature and also to comment on consumer culture. nicoledextras.com
https://vimeo.com/41578101

Emmanuelle Holmes Plant-dyed artisan wool and silk scarves created in a beautiful range of plant colours, prints and patterns that celebrate our natural Australian flora. Each scarf is one of a kind and original, handmade in Noosa using nature's hidden seacoast palette.
https://vimeo.com/105530446

https://vimeo.com/258179722





...Machine building it will be definetly something like this.

![](images/p3.jpg)

A machine that can transform nowadays fabrication processes: printing with fully biodegradable Materials


     [listen to the fabrics]  

       [lab-grown materials]

          [body movement]
