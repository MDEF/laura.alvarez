#ELECTRONICS PRODUCTION

Group assignment
- Characterize the specifications of your PCB production process

Individual assignment
- Make an in-circuit programmer by milling the PCB, then optionally, trying other processes.

·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.

![](images/W5_Port.JPG)


This week is about to understand what is a ISP and why we build one. I am not even a basic user of this, so I am totally trying to understand what is going on from the very beginning in order to achieve more complex ways of working. Due to the FabAcademy program and in order to develop some complex ideas in the future concerning to my final project, I want to get immersed this week with the electronics production atmosphere and get to understand as much as possible.


_T H E O R Y in concepts_

For the understanding I am following the FabAcademy page from Óscar González Fernández (2018) as he really did a gorgeos work explaning all the process behind the electronics environment!

**Memories in a Microcontroller**
(3) [Datam memory] [Programm memory] [EEPROM]

![](images/W5_MM.png)


**Flash Memory: programm memory - code stored here// non-volatile**

What it basically does is saving the CODE. It is splited in two parts: the boot program and the application program.

· The boot program - boot flash section: executes everytime there is a reset, change the major part (Application section) of the flash, protected against deletion and it can not be rewritten unless one specifically means to do so. It is conncected to an outer world such as PC.

· The application program - application flash section

![](images/W5_FM.png)


**Implementation of the FabISP**

**Hardware implementation**


**Components in the FabISP**

This is what we need in terms of hardware to get the ATtiny44 programmed via ISP:

![](images/W5_C.JPG)


_____________________________________________


_P R A C T I C E_

· Manufacturing the FabISP

**File Preparation**

Download these two files:
http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.traces.png
http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.interior.png

The traces png file to be machined with a 1/64” tool:

![](images/W5_T.png)


To cut the board out of the copper layer, we will use a 1/32” tool on the following profile:

![](images/W5_I.png)


*tool:* fabmodules.org

FabModules: detect changes in the png files and generate a path in it for the machining process:

Generating the path in the traces file...

![](images/W5_CT.png)

Generating the path in the outline file...

![](images/W5_CO.png)


Behind the values...

- Speed: 3 (Safe Mode for begginers)
- Origin: X0, Y0 and Z0 are set to 0mm, in order to keep the same origin as the locally defined in the machine
- ZJog is set to 10mm, meaning that the machine will lift the tool 10mm while not cutting (Safe Mode for begginers)
- Xhome, Yhome, Zhome are set to 0mm, 152.2mm and 60.5mm. There are not specific numbers, there are just random ones. The most important one is the latter, it needs to have a value that makes the tool be positioned once the job is finnished without scratching the whole copper surface.


Some other values to look at... (we don't change them unless we are looking for an alternative result)

- Cut depth:
0.1mm (traces)
0.5mm (outline)

- offset: 4. It’s the number of offsets that the program will calculate out of the BLACK-WHITE transitions.
when it is -1 it will take away the copper part.


Once we have transformed the .png archives into .ml archives, we save and download them to the next step: milling the PCB


**Milling the PCB**

![](images/W5_G.gif)

*Machines:* Roland MonoFab SRM-20 or a Modela MDX-20

1. Select the end-mill

![](images/W5_EM.jpg)

*image from https://steemit.com/blog/@kidult/mit-fabacademy-week4-assignment-electronics-production*

2. Put the end-mill

![](images/W5_EMP.jpg)

*image from https://steemit.com/blog/@kidult/mit-fabacademy-week4-assignment-electronics-production*

3. Fix the copper board without overlapping the double tape (if dirty clean with some alcohol on a cloth the working area)

4. Position and zeroing

![](images/W5_P.png)


5. Time for cutting: traces - it will engrave, it has to appear a big amount of "powder remains"


![](images/W5_E.jpg)

5. Cleaning the powder remains with a small brush



*Milling more than one PCB at the same time...*

With the same dimensions as the one from above we can mill x3 PCBs at the same time by setting up again the X origin point to the beggining of the next Xorigin. We will have to repeat this with the 1/32 end-mill.

![](images/W5_3F.png)


*Failures*

As explained before, it can happen that when milling some failures came up. Be aware of the Zhome point in order to not scratch the whole copper surface, be aware of the errors that the system can make happen and enjoy your patience!

![](images/W5_F3.JPG)



**Soldering the components**
Grabbing the components from the store room at the FabLab and ordering them:


*Board Layout + list w/ components*

![](images/W5_S.png)


Steps for soldering:

1. Place everything that you will need before and tidy up your space

What you will need:

   - components ready
   - light
   - tweezers
   - welder
   - holder/tape for the board
   - solder
   - copper


2. Play some music in your headphones

3. Look at the board layout and at the components, understand the workflow before the hands-on

4. Soldering in your way. Following some advice and others experience about starting with the passive small components, then the ATtiny44, the pin header and the mini USB connector. My way was to start with the small components in order to solder it better and leave the big ones at the end. When soldering, I usually heat up the component space and at the same time I put some solder. When there is solder enough, I take it out and then drop it with the welder.

- Heat up the space from the component you are soldering
- Put some solder down (tinning)
- Take out the solder while the welder is melting the solder
- Put the component and then melt solder again, just to get it locked (anchoring)
- Solder the opposite side


*The result!*

![](images/W5_SF.jpg)


Be aware of...

- At least 2h soldering for the first time: patience
- Although I dindn't work with through the microscope, I really recommend it. Also it is important for your eyes to work with light.
- Grab a mask if you don't work through the microscope in order to avoid the welding fume
- Put solder as you work and take into account that you will need more to glue it properly to the board
- When soldering the USB try to glue it the tiny part from the component before, otherwise you will find trouble when gluying the whole part




**Using the PCB as a programmer**

Once you have the board ready, the next steps are:

1) Checking if there is conexion. If it sounds it means there is conexion available.

![](images/W5_PISP.jpg)

![](images/W5_CC.jpg)


2) Checking the soldering in the board:
GND to GND and V to V




3) Controllers: laptop

4) Windows Software / Drivers Install

Get and install avrdude / GCC software and dependencies and drivers:

- Download and Install WinAVR

- Download the drivers for your version of Windows

- Download the FabISP Firmware



5) Power the FabISP Board

The board needs power:

- mini USB connector for the FabISP to program is plugged into your computer

- a separate pogramer is plugged in to the 6-pin programming header (AVRISP)

http://www.ladyada.net/learn/avr/setup-win.html AVR Set up tutorial

If you are using the ATAVRISP2 programmer only. If you connect the programmer to the 6-pin programming header on your FabISP board and you get:

![](images/W5_PC.JPG)
*Note that in the first try we got a red light, what means that the board is not getting power - check for shorts.*

![](images/W5_GL.jpg)
*Here we got the green light what means that the header is soldered correctly, the board is getting power*

6) Edit the makefile:
- Extrude
- Makefile: open with atom (or another editor)
*This file is ''made for telling the system which programmer are you using..*

The Makefile is set up to work with the AVRISP2 by default. If you are using another programmer, you will need to edit the Makefile. I used atom to open the makefile.

If you have to make changes follow the next steps:

![](images/W5_MC.JPG)



7) Programm the FabISP:
- Open GitBash
- Navigate to the directory where you saved the FabISP firmware
- Type make clean
- Type make hex

![](images/W5_IP.JPG)


- Type make fuse
*Usually Windows users doesn't success further than here*

I didn't success to go to the next step as when typing *make fuse* it didn't find/recognize the drivers because they are not actualized

![](images/W5_FP.JPG)


- Programming following the steps that are described here for Windows:

http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/index.html#toolchain


8) Next program the board to be an ISP
Foto con los dos ISP y las piezas y el cable

Para arreglarlo lo que se ha hecho ha sido:
- GitBash en la carpeta del FabISP del Firmware
Foto del bash
- Edit the makefile
Foto de la linea del makefile cambiada
- GitBash:
Make fuse
Make programm
it's working!


9) Verify That Your ISP is working correctly


10) After You Have Programmed the Board:

- Remove the solder bridge as shown in the picture below. Now you can use it as a programmer to program other boards.
- You have to remove the 0 ohm resistor if you want to program target boards that don't use 5v voltage.

Ir al administrador de dispositivos a comprobar que el tiny aparece
No aparecía, por que tuvimos que quitar el jumper 0 de la foto y cambiar las resistencias por las de 1000 (había de 1001)
Ya aparece al hacer los cambios


Some other useful links:
- http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/windows_avr.html

- https://zadig.akeo.ie/
