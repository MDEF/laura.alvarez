#MOULDING AND CASTING

Group assignment
- Review the safety data sheets for each of your molding and casting materials
- Make and compare test casts with each of them

Individual assignment
- Design a 3D mould around the stock and tooling that you'll be using, machine it, and use it to cast parts.

·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.

I worked together with Alex in this assignment.

**Explain how you made the files for machining**
*Design the object.* We decided to design a sole. We used SolidWorks. Commands. As we were thinking about working with recycled plastic. We tryed different prototypes: living hinges and geometrical structures.

Foto de SolidWorks general, foto de los prototipos

*Designing the mold.* Prepare the mold for the object. We left X mm on the side and create a adjusted mold in order to save material. We made the mold with curved walls as it was going to be printed in the 3D printer with PLA.


Foto del molde final

We imported the 3d model as a .stl in order to be opened with cura. We setted up the parameters for printing the object as we did in the week of 3d printing. We printed first the prototypes and then the final design. We balanced the speed with the infill in order to save time but to make sure that everything is fully closed.

Foto de cura

**Show how you made your mold and cast the parts**
*Molding (1)*. We had in mind working with recycled plastic. This requieres a high-temperature molding, at least up to 330 °C. At that time, we had just one type available: MOLD MAX 60. The working time is 40 minutes and the curing time 24h.

Materials used:
- MOLD MAX 60*:
Mix ratio is: 100 A: 3 B by WEIGHT
Stir part-B thoroughly before mixing with part-A
- MANN EASE RELEASE

- Scale
- Containers
- DIY vacuum machine from FabLab

- Usual safety equipment

Steps:
- We prepared all the materials we were going to use

Mold preparation
- We fulled it with water and measured its volume
- We cleaned the mold
- We dry out the mold twice (make sure it is totally dryed)
- We sprayed with the EASE RELEASE properly

Measuring and mixing
- Premix partA (separately)
- Premix partB (separately)
- Mix both together: making sure that we scrape the sides and bottom of the mixing container several times until the rubber is uniform.

Vacuum Degassing
- Vacuum to eliminate any entrapped air in the silicone rubber, for 3 minutes.
foto

Pouring, curing and performance
- Pouring the mix carefully making sure the mix goes to all the little holes.
Vídeo/gif
- Curing at room temperature 23°C for 24h in order to proceed with demolding.  


*Molding (2)*. As the other didn't work, and we kept trying with the recycled plastic, we played a little bit using one that doesn't support as high temperatures as the other: MOLD STAR 30. The working time is 45 minutes and the curing time 6h.

Materials used:
-  MOLD STAR 30:
Mix ratio is: 1 A: 1 B by WEIGHT
Stir part-B thoroughly before mixing with part-A
- MANN EASE RELEASE

- Scale
- Containers
- DIY vacuum machine from FabLab

- Usual safety equipment

Steps:
- We prepared all the materials we were going to use

Mold preparation
- We fulled it with water and measured its volume
- We cleaned the mold
- We dry out the mold twice (make sure it is totally dryed)
- We sprayed with the EASE RELEASE properly

Measuring and mixing
- Premix partA (separately)
- Premix partB (separately)
- Mix both together: making sure that we scrape the sides and bottom of the mixing container several times until the rubber is uniform.

Vacuum Degassing
- Vacuum to eliminate any entrapped air in the silicone rubber, for 3 minutes.
foto

Pouring, curing and performance
- Pouring the mix carefully making sure the mix goes to all the little holes.
Vídeo/gif
- Curing at room temperature 23°C for 24h in order to proceed with demolding.  


*Casting*.
We decided to try out funding recycled plastic in the mold. It didn't work. Then we tryed with orange peels bioplastics mixture.


**Read the MSDS and datasheet of your products and understood how to use, how to mix, etc**
In order to understand the products and how to work with them, we looked at:
- The product overview in each case
- The technical overview
- The processing recommendations
- The instructions when measuring and mixing & pouring, curing and performance
- SAFETY FIRST!

Foto de esto


**Described your problems and how you fixed them**
- 3D printing prototypes without the mold - We decided to print the final design with the mold fixed. If not, we should have added a fixed mold once it's printed, which means much more work, time and probably it may not be that accurate.
- The MOLD MAX 60* didn't work out, it didn't dry at all - We figured out why it didn't dry and it was because of the time that had been oppened, both the partA and partB. We also had in mind about the amount used with partB as some people had trouble with it. It has to be extremly accurate and precised.
- The recycled plastic didn't work out. We decided to try out another thing. I will come back to this later.


**Cast your final part and repeat the process at least once**



**Include your design files and photos**
