#COMPUTER CONTROLLED MACHINING

Group assignment
- Test runout, alignment, speeds, feeds, and toolpaths for your machine

Individual project
- Make something big on a CNC machine.

*Documentación Alex*
·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.


**Explained how you made your files for machining (2D or 3D).**
Explain CAM process
**Shown how you made something BIG (setting up the machine, using fixings, testing joints, adjusting feeds and speeds, depth of cut etc).**
**Described problems and how you fixed them.**
**Included your design files and ‘hero shot’ photos of final object.**


#Process#

2. Measure material

[]  plywood  []    15mm   []  2400x1250  []

3. Design something BIG
This week is about testing the CNC and making something big. We, as the part of the MDEF Mater, are working in pairs. My colleage Alexander is working with me. It is nice that he had a project in mind so we can go straight to the making part without spending that much time designing.

What is going to be designed: car sharing post
Which programm is used: SolidWorks
Key commands in SolidWorks for the 3D design to be printed with the CNC:


**Designing in 2D**
For the design we used also SolidWorks, "ensamblaje" archive for fixing all the parts in one layer. For convenience, it's important to separate every milling strategy into different layers. Also the stock needs its own layer.

![](images/W8_P.jpeg)

dsfd

**Generate G-code**

![](images/W8_GC.JPG)

In order to generate the G-code I used RhinoCAM, first make sure you have your design with the stock's left-down corner on the [0,0] and the axis matching the axis of the milling machine.

foto G-Code

**Generate the stock for the material we are going to mill**
in this case a  2400x1250x15mm plywood

[ Usually, we would prepare two G-codes:
- one for the markings of the screws that will hold the wood to the sacrificial layer of the machine
- one including the rest of strategies


We are using just one bit (6mm) so we just generate 2 G-Codes as mentioned above. The first thing we are going to make is a test in order to check the best pocket size for the press-fit parts joints.
- Marking for screws
- Test (6mm): Dogbones (through), dogbones (pockets), pockets (through), pockets (pockets), profile cuts

How to chose the right bit? In this case, for this week assignment, we don't really have to worry about choosing the right bit because the 6mm bit is already setted in the RhinoCAM we have in the lab.]

G-CODE: Marking of the screws that will hold the wood to the sacrificial layer of the machine.
- The 6mm bit is already set in the RhinoCAM we have in the lab

G-CODE: rest of strategies
RhinoCAM - Machining Browser
- "Machining Operations">"2 Axis"
- 2 Axis operations
- "Engraving" for the screw marks:
CUT PARAMETRES (tolerance, natural, at top, 0 finish depth, 6 rough depth cut: (this value should be no higher than the diameter of the tool)., 0 finish depth cut)
SORTING (LOWER LEFT)
- "Minimum distance sorting" (this will be the same for all the strategies).
- Axis profiling: for the outline cuts, I chose a "Profiling" operation. We can chose outside or inside.

CUT PARAMETERS: Conventional, right, outside
CUT LEVELS: At top, total cut depth 16, rough depth 16, rough depth cut 6
ADVANCED CUT PARAMETERS: Triangular, number of bridges 4

Previsualize a misulation of the jobs: "Simulate" tab

We need to choose the format for the G-code that we will generate. 2 options of milling machine:
1) Shopbot
2) Precix
Post- machine- .nc format (depending on software)

#Hands-on#

1) Testing joints in order to improve the design if needed and to fabricate a better final piece. This test allows us to discover wich gap has to be used in order to achieve the best join fit.

*Adjusting the design for testing the joints*
![](images/W8_T.jpeg)

*Some improvements to the design: adding a board for a best joint-fit from one of the parts*
![](images/W8_J.png)

*Joints view in SolidWorks*
![](images/W8_1.jpeg)

![](images/W8_2.jpeg)

*Creating dogbones in the joints that configure the strcuture*

![](images/W8_3.jpeg)

![](images/W8_4.jpeg)

Improvements on Wednesday
- Create a rigid base plate.
![](images/CH.jpeg)

- Change the direction of the Dogbones: the way we were designing them didn't allow us to have the 6mm diameter. Here you can appreciate in the left one how it reduces the diameter if the dogbone is placed such as that. Be aware of it.
![](images/CHD.jpeg)

-Exporting to Rhino and final details:
Things that we had to change:
- Make sure there are not any duplicate curves. Command: SelDup.
- Make 2D in Top view. Make sure that when you make the 2D it is working in the Top view. If not, it will not work with the CNC.
- After making 2D, use the commands: ungroup, explode and join.
- Draw the circles for the screws.
- Export in format .dwg for the Rhino5 where the RhinoCAM is available.

![](images/W8_P.JPG)




Dejar de margen:
dónde la llave: 0.6mm más, por lo que las medidas serían 15.6mm
en el palo: 0.4mm más

foto de las pruebas y la que mejor que da

Miércoles: hacer una base y cambiar los dogbones


4. CAM -> Gcode generation

Now we have the exported the .dwg file in order to start working in RhinoCAM.
Program:
select what we are going to do (with what we will be working): milling


5. Gcode is checked by someone
6. Design milled
7. Design assembled
8. Design finished
