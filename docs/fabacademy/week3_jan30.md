#COMPUTER AIDED-DESIGN

Assignment

- Model (raster, vector, 2D, 3D, render, animate, simulate, ...) a possible final project, and post it on your class page.

·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.

![](images/W4_P.JPG)


#· Evaluate and select 2D and 3D software#

In this week the assignment is about testing 2D and 3D software. Understanding that this week is all about design in the computer, I am starting from a zero point as I don't have any experience with these softwares, the only contact I had with it was in a pre-course that was scheduled in order to get to know some programs. Through this course I could get in touch with the atmosphere of these softwares I didn't designed any full concept on it, though.

Now as I am starting from scratch I just wanted to get in touch with different programs which ones I can get different results and ways of working.
I have a strong interest in maths and how you can incorporate this discipline in many others different fields or approach through the maths process, so I would love to inmmerse into parametrized design (I want to learn a tool that can repiclate parametres) with programms as OpenCad, which allows you to work with Inkscape so then I would try two different ones and combine them; Grasshopper as it is incorporated in Rhino 6 so then I can work integrating both; and also Fusion360 as an starting point in parametrized design due to its easier way of working comparing to the other two.
I am interested also in the way that te materials can be shown with the softwares. They way that you can represent, for instance, nature in a 3D program really fascinates me, transparency and crystals.


**2D, 2.5D design**: for me this is like an expressive design in which its programms allows me to turn my ideas into action, to communicate them. It is interesting how you can draw with these techniques and compare, once you have fabricated the item, how it has changed, if it did.

For first what I did is trying to understand what every concept means...

- raster: a raster graphics or bitmap image is a dot matrix data structure that represents a generally rectangular grid of pixels (points of color), viewable via a monitor, paper, or other display medium. A raster is technically characterized by the width and height of the image in pixels and by the number of bits per pixel (or color depth, which determines the number of colors it can represent).

Applications: Computer displays, Image storage and Geographic information systems.
Resolution: Raster graphics are resolution dependent. This property contrasts with the capabilities of vector graphics, which easily scale up to the quality of the device rendering them. Raster graphics deal more practically than vector graphics with photographs and photo-realistic images, while vector graphics often serve better for typesetting or for graphic design.

Raster-based image editors: Raster-based image editors revolve around editing pixels, unlike vector-based image editors, such as Xfig, CorelDRAW, Adobe Illustrator, or Inkscape, which revolve around editing lines and shapes (vectors). When an image is rendered in a raster-based image editor, the image is composed of millions of pixels. At its core, a raster image editor works by manipulating each individual pixel.[8] Most[citation needed] pixel-based image editors work using the RGB color model, but some also allow the use of other color models such as the CMYK color model.

Raster-based image editors: scan, GIMP, BIMP, Photoshop, MyPaint, Krita, ImageMagick, GraphicsMagick, Encoding.

*GIMP* image and then lasercut. Good for things to be expressive: fully pixeled
[High Quality Photo Manipulation] [Original Artwork Creation] [Graphic Design Elements] [Programming Algorithms]
*MyPaint* illustration
*ImageMagic* process images: change format, vector to raster, resize pictures from the camera with resolution and good results.

- vector: Vector graphics are computer graphics images that are defined in terms of 2D points, which are connected by lines and curves to form polygons and other shapes.[1] Each of these points has a definite position on the x- and y-axis of the work plane and determines the direction of the path;

Properties: values for stroke color, shape, curve, thickness, and fill.

Vector-based image editors:: Inkscape video, lodraw, Illustrator, CorelDRAW, Scribus, QCAD, FreeCAD video, Layout.

*Inkscape* drawing geometrical objects. cut out many cuts at the same time, useful for the laser cutting week.
*FreeCAD* it allows to 3D drawings too.

Vector images can be rasterized (converted into pixels), and raster images vectorized (raster images converted into vector graphics), by software. In both cases some information is lost, although vectorizing can also restore some information back to machine readability, as happens in optical character recognition.


**3D design**: for these type of programms I went into how to understand geometry, understand the different types and its conceptos which allows me to reflection in a better way. It is quite powerful cause it creates such as the power function represetantions of my idea and of what I want to present.

- types: create files that represent points in space, with plenty of geometry like triangles, polygons, and curves to tie the points together into a three-dimensional object.

Constructive Solid Geometry, constrained, hierarchical, parametric, procedural, algorithmic
boundary (b-rep), function (f-rep) representations
GUIs, scripting, hardware description languages
imperative, declarative, generative, optimization, Multidisciplinary Design Optimization

"3D modeling is equal parts math, geometry, and design. Using specialized software, 3D modeling generates files that are essentially instructions for 3D printers. Like a sculptor, architect, or builder, modeling demonstrates how an object or building will be constructed. If a blueprint is a 2D representation of a building, a model is its 3D representation, giving a mathematical description of the surface of that object"

- programs
*Rhino Grasshopper*: rhino allows to design whatever you are looking for and also it has the advantage of Grasshopper, which is quite useful to end up the PIECES
*OpenSCAD*: is it quite easy to work simultaneously with programs such as Inkscape
*Fusion360*: it gives the chance to start working with parametrized design easily



#·Demonstrate and describe processes used in modelling with 2D and 3D software#

For this week I decided to try out different softwares and try to design an organic shoe as I am fascinated about them.

Organic shoes appear when biodegradable materials are transformed into shoes. Some brands have been exploring and developing these new type of designing shoes. Beauty speaks by itself.

![](images/W3_1.png)

I want to enhance the material, its texture as it is.


2D software

*Inkscape*
1. Downnloaded the pattern
2. Take the pattern, the graphic image, and save it w/ a graphic format PNG
3. Open a new document - File - Document properties - Change the letter format to a tabloid format
4. Import the file
5. Embeded
6. Correct size relative to the page
7. Ctrl + shift + L: open the layers dialogue
8. Create 1. Pattern Generator + 2.Vamp

![](images/W3_2.png)

_
ADDING CURVES
Tool: Pen tool
Understanding how to draw curves with the guidelines
Working with the pencil - Following the guidelines - turning off the layer of the pattern generator - create the curves

Create the curves: Tool: Edit path by nodes + select and transform objects + make selected nodes symetric

DRAWING THE QUARTERS
Ctrl + shift + F: stroke style: get out the stroke style and get the dot line : convinience to make it easier to visualize
Zoom to fit drawing in window: an offset for the overlab?
Ctrl + D: duplicate
Layer - move to layer above

OTHER PATTERN PIECES: wing tip, vamp top, new wing tip, new vamp top, heel, new quarter, quarter


Adding material: texture

Creating triangles with texture filter + bark, 3D wood and picking the colors from the image.

![](images/W3_OS.JPG)

![](images/W3_UN.JPG)

_



3D software

*Rhino*

![](images/W3_4.JPG)

_
Drawing a bottle
1. Design the left part from the bottle
2. Create the surface: Select and command surface from 3 or 4 corner points - Revolve - Full Circle
3. Convert to solid: Select the bottle - Fillet surface - offset surface

How to render - Basics
- 3D models
- Environment + lights
- Materials

Auxpecker: visualize materials while Working
Brazil
Maxwell
V Ray
Penguin: sketch

![](images/W3_5.png)
_



*Rhino Grasshopper*

![](images/W3_6.JPG)

How to model a shoe's midsole using just one surface, plus adding patterns via Grasshopper!

1. Modelling the midsole with Rhinoceros

![](images/W3_7.png)
![](images/W3_8.JPG)
![](images/W3_9.JPG)
![](images/W3_10.JPG)
![](images/W3_11.JPG)
![](images/W3_12.JPG)
![](images/W3_13.JPG)

2. Adding patterns via Grasshopper

![](images/W3_14.JPG)
![](images/W3_15.JPG)
![](images/W3_16.JPG)
![](images/W3_17.JPG)
![](images/W3_18.JPG)

techniques
Surfaces in Rhino - Nerv Surface with 4 corners - Flat plain nerv surface
Surfaces always have u and v cornet, curves that define the sections in two directions (X,Y). This is important because in Grasshopper is going to take the info about the surface and the cornets and is going to use the information to apply the pattern to the result.

feeding geomtry from rhino into Grasshopper - important to make difference between a surface and a polisurface

IMPORT GEOMETRY INTO Grasshopper

https://www.food4rhino.com/
Download an app called lunchbox  - install plug in in Grasshopper
LunchBox is a plug-in for Grasshopper for exploring mathematical shapes, paneling, structures, and workflow.  We have also introduced new components for general machine learning implementations such as regression analysis, clustering, and networks.

Create surface - Pannels - Hexagon cells  (surf, U, V, t)




I love: ¡Generative Design.

*Generative Design*
"Generative design is an iterative design process that involves a program that will generate a certain number of outputs that meet certain constraints, and a designer that will fine tune the feasible region by changing minimal and maximal values of an interval in which a variable of the program meets the set of constraints, in order to reduce or augment the number of outputs to choose from."

![](images/W3_GD.png)

Its beauty is in itsm features:
- it  doesn't need to be run on a machine like a digital computer, it can be run by a human for example with pen and paper
- the designer learns to refine the program with each iteration as his design goals become better defined over time
- the output could be images, sounds, architectural models, animation
- fast method of exploring design possibilities


The process combined with the power of digital computers that can explore a very large number of possible permutations of a solution enables designers to generate and test brand new options, beyond what a human alone could accomplish, to arrive at a most effective and optimized design. It mimics nature’s evolutionary approach to design through genetic variation and selection.


The designer doesn't need to be a human, it can be a test program in a testing environment or an artificial intelligence (see for example Generative adversarial networks):

"Generative adversarial networks (GANs) are a class of artificial intelligence algorithms used in unsupervised machine learning, implemented by a system of two neural networks contesting with each other in a zero-sum game framework. They were introduced by Ian Goodfellow et al. in 2014.[1] This technique can generate photographs that look at least superficially authentic to human observers, having many realistic characteristics (though in tests people can tell real from generated in many cases)"

It is been used in various design fields such as art, architecture, communication design, and product design

![](images/W3_GD1.png)

_________________________

Next steps: GIMP, Photoshop, illustrator, SolidWorks, Fusion 360, Premiere After Effects, encoding.

_________________________



**Files**
