#ELECTRONICS DESIGN

Group assignment
- Use the test equipment in your lab to observe the operation of a microcontroller circuit board

Individual assignment
- Redraw the echo hello-world board, add (at least) a button and LED (with current-limiting resistor), check the design rules, make it, test it.
Extra credit: Simulate its operation. Render it.

·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.

This week was about:
- use the test equipment in your lab to observe the operation of a microcontroller circuit board
- redraw the echo hello-world board, add (at least) a button and LED (with current-limiting resistor)Resistor calculator, check the design rules, make it, and test it
*extra credit: measure its operation
extra extra credit: simulate its operation*

Learning outcome:
Select and use software for circuit board design. YES
Demonstrate workflows used in circuit board design. YES

**Show your process using words/images/screenshots**
**Explained the proglems and how you fiex them**
**Done fabbercise today**
**Included original design files**

Drawing the board...

Software used: Eagle, why? Because it's quite easier to use than KidCad for begginners although its interface it's pretty shit.

Some useful links:
Download eagle
https://www.autodesk.com/products/eagle/free-download

Download library: Download and install the Fab Academy component libraries
https://hackmd.io/60f1XrRfTsqSgWNKY701LA

![](images/W7_EL.JPG)


List of the components:

1) **Microcontroller - Attiny44SSU:** Difference of potential energy between two pointOnce the microcontroller is programmed, the program stored in non-volatile memory, allowing itself to remember the program.
2) **FTDI-SMD-HEADER - 1X06SMD:** Powers the board and allows board to talk to computer.
3) **AVRISP** 6-pin Programming Header: Used to program the board.
4) **LEDFAB1206** (Light Emitting Diode): LEDs have polarity - the side with the line is the cathode and connects to the ground side.
5) **Capacitor 10 pF**
6) **Capacitor 10 pF**
7) **Crystal - 20MHz:** Difference of potential energy between two points. It is measuredExternal clock. The Attiny has a 8Mhz clock but the resonator is faster (increases the clock speed of the processor) and more accurate.
8) **OMRON SWITCH - 6MM_SWITCH**
6) Resistor - value 10k ohm: Purpose: pull-up resistor.
7) Resistor - value 499 ohms: Purpose: current limiting resistor for LED
8) **Ground**
9) **VCC**


Inside of EAGLE.

Steps: 1) File - Project - Name project   2) File - Schematic  3) Library - Open library manager - Library available - Browse - fab - Use

Take into acount: GND + VCC

To Add components go into the SCHEMATIC FILE

The list of the components that you can find in the fab library are these ones:

![](images/W7_AC.JPG)

>6MM_SWITCH
> 1x ATTINY44
> 1x VRISP: Footprint: 2X03
> 2x CAP-US: Footprint: C1206FAB
> 1x CRYSTAL: Footprint: 2-SMD-5X3MM
> 1x FTDI-SMD-HEADER: Footprint: 1X06SMD
> 1x LED: Footprint: LED1206FAB
> 1x RES-US: Footprint: R1206FAB


Commands to use:

- Add part: search for fab library and then the corcerning component
- Net: create conexions (green line): connects one component to the other
- Add Label: (AB): text command to label the traces.
- Name: (R2): To connect components located at a distance from one another, “name” command allows to change the name of the trace and connect it to another component.

![](images/W7_BE.JPG)



You can visualize in the board layout once you have added a component in the schematic file:

![](images/W7_V.JPG)


ERC command: checking for connectivity with an Electrical Rule Check:

![](images/W7_ERC.JPG)

Reorganizing:

![](images/W7_B1.PNG)


Layout that came up in Eagle:

![](images/W7_B2.PNG)


Export:
- Hide all the layers

Create the milling strategy in mods of fabmodules (same process as electronics production week)
- milling
- soldering
- programming

Before programming, the digital multimeter (DMM) is useful for checking that all our connections are working and detecting possible bad connections. Turn the multimeter into the Continity Test Mode. With the circuit de-energized, place the test leads across the component being tested. It will beep if a complete path (continuity) is detected. If the circuit is open the DMM will not beep, therefore we will know there is a bad connection between one multimeter lead and the other.

![](images/W7_FD.jpg)
